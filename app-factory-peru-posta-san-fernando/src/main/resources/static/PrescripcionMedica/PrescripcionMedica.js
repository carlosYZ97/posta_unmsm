var PrescripcionModule = (function () {
	
	 var obj = this;

	    globalFunctions.extendKORequiredString(ko);
	    globalFunctions.configTools(ko);
	    
	    var config = function (prPaciente) {
	        ko.applyBindings(obj, document.getElementById(prPaciente));
	    };
	    
	    var configView = function (prPaciente) {
	        $("").showDiv({ section: 'secPrescripcion', divId: prPaciente });
	    }

	    
	var PrescripcionDTO = function (){
		var _diagnostico = ko.observable("");
		var _CIE10 = ko.observable("");
		return {
			diagnostico:_diagnostico,
			CIE10: _CIE10
		}
	}
	    
	var bto_addRow = function (){ addRow();}
	var bto_deleteRow = function (){ deleteRow();}
	var aPrescripcionDTO = new PrescripcionDTO();
	
	var bto_rewardClick = function (){rewardClick();}
	
	var rows = 0;
	
	var addRow = function (){
		rows++;
		var html = "<tr>";
			html += "<td>"+rows+"</td>";
			html += "<td><input type='text' id='medicamento' data-bind='value: aPrescripcionModule.aPrescripcionDTO.medicamento' ></td>";
			html += "<td><input type='text' id='concentracion' data-bind='value: aPrescripcionModule.aPrescripcionDTO.concentracion'></td>";
			html += "<td><input type='text' id='FF' data-bind='value: aPrescripcionModule.aPrescripcionDTO.FF' style='width:40px'></td>";
			html += "<td><input type='number' id='cantidad' min='0' data-bind='value: aPrescripcionModule.aPrescripcionDTO.cantidad' style='width:40px'></td>";
			html += "<td><input type='text' id='dosis' data-bind='value: aPrescripcionModule.aPrescripcionDTO.dosis'></td>";
			html += "<td><input type='text' id='via' data-bind='value: aPrescripcionModule.aPrescripcionDTO.via' style='width:60px'></td>";
			html += "<td><input type='text' id='frecuencia' data-bind='value: aPrescripcionModule.aPrescripcionDTO.frecuencia' style='width:40px'></td>";
			html += "<td><input type='text' id='duracion' data-bind='value: aPrescripcionModule.aPrescripcionDTO.duracion' style='width:70px'></td>";
			html += "<td>" +
					"<div class='btn btn-danger' data-bind: 'click: aPrescripcionModule.bto_deleteRow'><i class='fa fa-trash'></i></div>" +
					"</td>";
			html += "</tr>";
		
		document.getElementById("tbody").insertRow().innerHTML = html;
		$('.btn-danger').on('click', deleteRow);
	}
	
	function deleteRow (){
		$(this).parent().parent().fadeOut("slow", function (){$(this).remove();});
	}
	
	var rewardClick = function (){
		$("").showDiv({ section: 'secBusquedaConsulta', divId: 'CreateConsultaMain' });
	}
	
	return {
		configView: configView,
		config: config,
		bto_addRow: bto_addRow,
		bto_rewardClick: bto_rewardClick,
		aPrescripcionDTO: aPrescripcionDTO
		
	}
	    
});