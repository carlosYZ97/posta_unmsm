var BusquedaTipoUsuarioModule = (function () {
	var obj = this;
	globalFunctions.extendKORequiredString(ko);
    globalFunctions.configTools(ko);
    
    var config = function (prSearchTipoUsuario) {
        ko.applyBindings(obj, document.getElementById(prSearchTipoUsuario));
        $("").showDiv({ section: prSearchTipoUsuario, divId: 'TiposUsuariosSearchMain' });
    };
    
    
    var KDStatus = {
    		Inactivo: 0,
    		Activo: 1
    }
    
    var aBusquedaTipoUsuarioEntityCollection =  ko.observableArray([]);
    
    var FilterTipoUsuarioDTO = function(){
    	
    	var _estado = ko.observable(-1);
    	var _nombre = ko.observable("");
    	var _descripcion = ko.observable("");
    	
    	return{
    		estado: _estado,
    		nombre: _nombre,
    		descripcion: _descripcion
    	}
    }
    
    var aFilterTipoUsuarioDTO = new FilterTipoUsuarioDTO();
    
    var bto_searchTipoUsuarios = function (){
    	searchTipoUsuarios();
    }
    
    var bto_newTipoUsuario = function (){
    	newTipoUsuario();
    }
    
    var bto_limpiar = function(){
    	clearFields();
    }
    
    var bto_showTipoUsuario = function(prIndex, data){
    	ShowTipoUsuario(prIndex, data);
    }
    
    var btn_ActivateDesactivate_Click = function (prIndex, data) { activateOrDesactivateTipoUsuario(prIndex, data); }
    
    var ShowTipoUsuario = function (prIndex, prData) {

        var onSucessLoad = function() {
            $("").showDiv({ section: 'secBusquedaTipoUsuario', divId: 'divCallTipoUsuario' });
            aTipoUsuarioModule.configView("divTipoUsuarioMain");
            aTipoUsuarioModule.setTipoUsuario(prData, prIndex);

        };

        $("").coLoadDiv({
            action: "/tipoUsuario/tipoUsuarioView",
            cbhSucess: onSucessLoad,
            splashscreen: true,
            elementID: "#divCallTipoUsuario"
        }).callService();
    };
    
    var searchTipoUsuarios = function (){
    	
    	var cbhSucess = function (data) {
    		aBusquedaTipoUsuarioEntityCollection([]);
    		aBusquedaTipoUsuarioEntityCollection(data);
        }

        var cbhError = function (data) {
            $.growl.error(null, 'Error', data.responseText);
        }

        var lParameter = "prTipoUsuarioDTO="+ko.mapping.toJSON(aFilterTipoUsuarioDTO);
        
    	 $("").coserver({
             action: "/tipoUsuario/tipoUsuarioList",
             splashscreen: true,
             cbhSucess: cbhSucess,
             cbhError: cbhError
         }).callService(lParameter);
    }
    
    var newTipoUsuario = function(){
    	
    	var onSucessLoad = function(){
             $("").showDiv({ section: 'secBusquedaTipoUsuario', divId: 'divCallTipoUsuario' });
             aTipoUsuarioModule.configView("divTipoUsuarioMain");
    	}
    	
    	 $("").coLoadDiv({
             action: "/tipoUsuario/tipoUsuarioView",
             cbhSucess: onSucessLoad,
             splashscreen: true,
             elementID: "#divCallTipoUsuario"
         }).callService();
    }
    
    var activateOrDesactivateTipoUsuario = function (prIndex, data) {

    	var setSuccess = function (dataRol) {
    		searchTipoUsuarios();
            $.growl.notice(null, 'Éxito', 'Tipo Usuario actualizado con éxito.');
        }

        var lParametros = "prTipoUsuarioId=" + data.tipoUsuarioId;
        $("").coserver({
            action: "/tipoUsuario/activateOrDesactivateTipoUsuario", splashscreen: true, cbhSucess: setSuccess, cbhError: onErrorRol
        }).callService(lParametros);
    }
    
    var onErrorRol = function (data) {
        $.growl.error(null, 'Error', (data[0] == null ? data.responseText : data[0].responseText));
    };
    
    var clearFields = function(){
    	aFilterTipoUsuarioDTO.estado(-1);
    	aFilterTipoUsuarioDTO.nombre("");
    	aFilterTipoUsuarioDTO.descripcion("");

    }
	
	return {
		 config: config,
		 bto_searchTipoUsuarios: bto_searchTipoUsuarios,
		 bto_newTipoUsuario:bto_newTipoUsuario,
		 KDStatus:KDStatus,
		 aBusquedaTipoUsuarioEntityCollection:aBusquedaTipoUsuarioEntityCollection,
		 aFilterTipoUsuarioDTO: aFilterTipoUsuarioDTO,
		 bto_limpiar:bto_limpiar,
		 bto_showTipoUsuario:bto_showTipoUsuario,
		 btn_ActivateDesactivate_Click:btn_ActivateDesactivate_Click
	}
	
});