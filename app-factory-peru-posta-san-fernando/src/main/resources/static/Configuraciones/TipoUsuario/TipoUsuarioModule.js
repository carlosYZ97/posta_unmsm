var TipoUsuarioModule = (function() {
	var obj = this;

	globalFunctions.extendKORequiredString(ko);
	globalFunctions.configTools(ko);

	var config = function(prTipoUsuario) {
		ko.applyBindings(obj, document.getElementById(prTipoUsuario));
	};

	var configView = function(prTipoUsuario) {
		$("").showDiv({
			section : 'secTipoUsuario',
			divId : prTipoUsuario
		});
	}
	
	var TipoUsuarioDTO = function() {

		var _tipoUsuarioId = ko.observable(0);
		var _nombre = ko.observable("");
		var _descripcion = ko.observable("");
		var _estado = ko.observable(1);
		var _creator = ko.observable("");
		var _createdDescripcion = ko.observable("");

		return {
			tipoUsuarioId: _tipoUsuarioId,
			nombre: _nombre,
			descripcion: _descripcion,
			creator : _creator,
			estado : _estado,
			createdDescripcion : _createdDescripcion
		}
	}
	
	var bto_addTipoUsuario = function() {
		addTipoUsuario();
	}

	var bto_rewardClick = function() {
		rewardClick();
	}

	var bto_clearFields = function() {
		clearFields();
	}
	
	var bto_updateTipoUsuario = function(){
		updateTipoUsuario();
	}
	

	var aTipoUsuarioDTO = new TipoUsuarioDTO();

	var addTipoUsuario = function() {

		var cbhSucess = function(data) {
			$.growl.notice(null, 'Éxito',
					'El tipo de usuario fue registrado con éxito.');

			rewardClick();
		}

		var cbhError = function(data) {
			$.growl.error(null, 'Error', data.responseText);
		}

		var lParameter = "prTipoUsuarioDTO=" + ko.mapping.toJSON(aTipoUsuarioDTO);

		$("").coserver({
			action : "/tipoUsuario/tipoUsuarioAdd",
			splashscreen : true,
			cbhSucess : cbhSucess,
			cbhError : cbhError,
		}).callService(lParameter);
	}

	var updateTipoUsuario = function() {

		var cbhSucess = function(data) {
			$.growl.notice(null, 'Éxito',
					'El tipo de usuario fue actualizado con éxito.');

			rewardClick();
		}

		var cbhError = function(data) {
			$.growl.error(null, 'Error', data.responseText);
		}

		var lParameter = "prTipoUsuarioDTO=" + ko.mapping.toJSON(aTipoUsuarioDTO);

		$("").coserver({
			action : "/tipoUsuario/tipoUsuarioAdd",
			splashscreen : true,
			cbhSucess : cbhSucess,
			cbhError : cbhError,
		}).callService(lParameter);
	}

	var rewardClick = function() {
		$("").showDiv({
			section : 'secBusquedaTipoUsuario',
			divId : 'TiposUsuariosSearchMain'
		});
	}

	var clearFields = function() {
		aTipoUsuarioDTO.tipoUsuario(0);
		aTipoUsuarioDTO.nombre("");
		aTipoUsuarioDTO.descripcion("");
		aTipoUsuarioDTO.creator("");
		aTipoUsuarioDTO.createdDescripcion("");
	}

	var setTipoUsuario = function(prData, prIndex) {
		aTipoUsuarioDTO.tipoUsuarioId(prData.tipoUsuarioId);
		aTipoUsuarioDTO.nombre(prData.nombre);
		aTipoUsuarioDTO.descripcion(prData.descripcion);
		aTipoUsuarioDTO.creator(prData.creator);
		aTipoUsuarioDTO.estado(prData.estado);
		aTipoUsuarioDTO.createdDescripcion(prData.createdDescripcion);

	};

	return {
		config : config,
		configView: configView,
		bto_addTipoUsuario : bto_addTipoUsuario,
		bto_rewardClick : bto_rewardClick,
		setTipoUsuario : setTipoUsuario,
		bto_clearFields : bto_clearFields,

		aTipoUsuarioDTO : aTipoUsuarioDTO,
		bto_updateTipoUsuario: bto_updateTipoUsuario,
	}
});