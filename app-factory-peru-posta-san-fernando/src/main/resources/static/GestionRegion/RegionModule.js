var configView = function (prLocation, ModeEdit) {
        $("").showDiv({ section: 'sec', divId: prLocation });
        loadRegion(KDRegion.Departament, 0);
        aModeEdit(ModeEdit);
    }
	
	var Region = function () {
        var _DEPARTAMENTO = ko.observable(0);
        var _PROVINCIA = ko.observable(0);
        var _DISTRITO = ko.observable(0);
        return {
            DEPARTAMENTO: _DEPARTAMENTO,
            PROVINCIA: _PROVINCIA,
            DISTRITO: _DISTRITO
        }
    };
	
	var aListDepartamentoViewModel = ko.observableArray([]);
    var aListProvinciaViewModel = ko.observableArray([]);
    var aListDistritoViewModel = ko.observableArray([]);

    var KDRegion = {
        Departament: 0,
        Provincia: 1,
        District: 2
    }
	
	
	var eventGetProvincia = function () {

        cleanRegion();
        if (aRegion.DEPARTAMENTO() > 0) {
            $("#edProvincia").attr("disabled", false);
            loadRegion(KDRegion.Provincia, aRegion.DEPARTAMENTO());
        }
    };

    var eventGetDistrito = function () {
        if (aRegion.PROVINCIA() > 0) {
            $("#edDistrito").attr("disabled", false);
            loadRegion(KDRegion.District, aRegion.PROVINCIA());
        }
    };

    var loadRegion = function (RegionType, RegionID) {
        var setSucess = function (data) {
            if (RegionType == KDRegion.Departament) aListDepartamentoViewModel(data);

            if (RegionType == KDRegion.Provincia) aListProvinciaViewModel(data);

            if (RegionType == KDRegion.District) aListDistritoViewModel(data);
        }

        var onError = function (data) {
            $.growl.error(null, 'Error', data.astMessage);
        }

        var lParametros = 'prRegionType=' + RegionType;
        lParametros += '&prRegionID=' + RegionID;

        $("").coserver({
            action: "//",
            cbhSucess: setSucess,
            cbhError: onError,
            splashscreen: true
        }).callService(lParametros);
    }
	
	
	
	    var cleanRegion = function () {
        aRegion.PROVINCIA(0);
        aRegion.DISTRITO(0);
        aListProvinciaViewModel([]);
        aListDistritoViewModel([]);
        $("#edProvincia").attr("disabled", true);
        $("#edDistrito").attr("disabled", true);
    }