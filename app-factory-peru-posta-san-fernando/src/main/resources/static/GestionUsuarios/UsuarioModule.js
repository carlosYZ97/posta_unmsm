var UsuarioModule = (function() {
	var obj = this;

	globalFunctions.extendKORequiredString(ko);
	globalFunctions.configTools(ko);

	var config = function(prUsuario) {
		ko.applyBindings(obj, document.getElementById(prUsuario));
		configAutoComplete();
	};

	var configView = function(prUsuario) {
		$("").showDiv({
			section : 'secUsuario',
			divId : prUsuario
		});
	}

	var aListRoles = ko.observableArray([]);

	var UsuarioDTO = function() {

		var _usuarioId = ko.observable(0);
		var _usuario = ko.observable("");
		var _contrasena = ko.observable("");
		var _confirmacionContrasena = ko.observable("");
		var _nombre = ko.observable("");
		var _apellidoPaterno = ko.observable("");
		var _apellidoMaterno = ko.observable("");
		var _tipoDocumento = ko.observable(-1);
		var _numeroDocumento = ko.observable("");
		var _fechaNacimientoDescripcion = ko.observable("");
		var _correo = ko.observable("");
		var _telefono = ko.observable("");
		var _celular = ko.observable("");
		var _estado = ko.observable(1);
		var _creator = ko.observable("");
		var _createdDescripcion = ko.observable("");
		var _roles = ko.observableArray([]);

		return {
			usuarioId : _usuarioId,
			usuario : _usuario,
			contrasena : _contrasena,
			confirmacionContrasena : _confirmacionContrasena,
			nombre : _nombre,
			apellidoPaterno : _apellidoPaterno,
			apellidoMaterno : _apellidoMaterno,
			tipoDocumento : _tipoDocumento,
			numeroDocumento : _numeroDocumento,
			fechaNacimientoDescripcion : _fechaNacimientoDescripcion,
			correo : _correo,
			telefono : _telefono,
			celular : _celular,
			creator : _creator,
			estado : _estado,
			createdDescripcion : _createdDescripcion,
			roles : _roles
		}
	}

	var RolDTO = function() {
		var _rolId = ko.observable(0);
		var _name = ko.observable("");
		var _descripcion = ko.observable("");
		var _estado = ko.observable(1);
		var _creator = ko.observable("");
		var _createdDescripcion = ko.observable("");

		return {
			rolId : _rolId,
			name : _name,
			descripcion : _descripcion,
			creator : _creator,
			estado : _estado,
			createdDescripcion : _createdDescripcion
		}
	}

	var bto_addUser = function() {
		addUser();
	}

	var bto_rewardClick = function() {
		rewardClick();
	}

	var bto_clearFields = function() {
		clearFields();
	}

	var bto_updateUser = function() {
		updateUser();
	}

	var bto_addRol = function() {
		addRol();
	}

	var bto_rewardClickToUser = function() {
		rewardClickToUser();
	}
	
	var bto_addRolToUser = function (){
		addRolToUser();
	}
	
	var bto_removeRolClick = function(){
		removeRolClick();
	}
	
	var aUsuarioDTO = new UsuarioDTO();
	var aRolDTO = new RolDTO();

	 var configAutoComplete = function () {
		 acoacRol = $("#coacRol").coac({ action: "/search/getRolesUser", elemselected: autoCompleteRol, elemremoved: elemremovedRol, width: "100%" });
	 }
	 
	 var autoCompleteRol = function(id, value, image){
		 aRolDTO.name(value);
		 aRolDTO.rolId(id);
		 acoacRol.ItemSelected(id,value,image);
	 }
	 
	 var elemremovedRol = function(){
		 aRolDTO.name("");
		 aRolDTO.rolId(0);
		 acoacRol.RemoveSelected();
	 }
	
	var addUser = function() {

		var cbhSucess = function(data) {
			$.growl.notice(null, 'Éxito',
					'El usuario fue registrado con éxito.');

			rewardClick();
		}

		var cbhError = function(data) {
			$.growl.error(null, 'Error', data.astMessage);
		}

		var lParameter = "prUserDTO=" + ko.mapping.toJSON(aUsuarioDTO);

		$("").coserver({
			action : "/user/userAdd",
			splashscreen : true,
			cbhSucess : cbhSucess,
			cbhError : cbhError,
		}).callService(lParameter);
	}

	var updateUser = function() {

		var cbhSucess = function(data) {
			$.growl.notice(null, 'Éxito',
					'El usuario fue actualizado con éxito.');

			rewardClick();
		}

		var cbhError = function(data) {
			$.growl.error(null, 'Error', data.astMessage);
		}

		var lParameter = "prUserDTO=" + ko.mapping.toJSON(aUsuarioDTO);

		$("").coserver({
			action : "/user/userAdd",
			splashscreen : true,
			cbhSucess : cbhSucess,
			cbhError : cbhError,
		}).callService(lParameter);
	}

	var rewardClick = function() {
		$("").showDiv({
			section : 'secBusquedaUsuario',
			divId : 'UsersSearchMain'
		});
	}

	var addRol = function() {

		$("").showDiv({
			section : 'secUsuario',
			divId : 'RolUsuarioMain'
		});

	}

	var rewardClickToUser = function() {
		$("").showDiv({
			section : 'secUsuario',
			divId : 'divUsuarioMain'
		});
	}
	
	var addRolToUser = function (){
		
		aListRoles.push(aRolDTO);
		aUsuarioDTO.roles([])
		aUsuarioDTO.roles(aListRoles());
		
		var cbhSucess = function(data) {
			$.growl.notice(null, 'Éxito',
					'El rol fue añadido con éxito.');
			aListRoles([]);
			ko.utils.arrayForEach(data.roles, function(lItem) {

				var aRolDTO = new RolDTO();
				aRolDTO.rolId(lItem.rolId);
				aRolDTO.name(lItem.name);
				aRolDTO.descripcion(lItem.descripcion);
				aRolDTO.creator(lItem.creator);
				aRolDTO.estado(lItem.estado);
				aRolDTO.createdDescripcion(lItem.createdDescripcion)
				aListRoles.push(aRolDTO);
			});
			elemremovedRol();
		}

		var cbhError = function(data) {
			$.growl.error(null, 'Error', data.responseText);
		}

		var lParameter = "prUserDTO=" + ko.mapping.toJSON(aUsuarioDTO);

		$("").coserver({
			action : "/user/userRolAdd",
			splashscreen : true,
			cbhSucess : cbhSucess,
			cbhError : cbhError,
		}).callService(lParameter);
	}
	
	var removeRolClick = function(){
		
	}

	var clearFields = function() {
		aUsuarioDTO.usuarioId(0);
		aUsuarioDTO.usuario("");
		aUsuarioDTO.contrasena("");
		aUsuarioDTO.confirmacionContrasena("");
		aUsuarioDTO.nombre("");
		aUsuarioDTO.apellidoPaterno("");
		aUsuarioDTO.apellidoMaterno("");
		aUsuarioDTO.numeroDocumento("");
		aUsuarioDTO.tipoDocumento(-1);
		aUsuarioDTO.numeroDocumento("");
		aUsuarioDTO.fechaNacimientoDescripcion("");
		aUsuarioDTO.correo("");
		aUsuarioDTO.telefono("");
		aUsuarioDTO.celular("");
		aUsuarioDTO.creator("");
		aUsuarioDTO.createdDescripcion("");
	}

	var setUsuario = function(prData, prIndex) {
		aUsuarioDTO.usuarioId(prData.usuarioId);
		aUsuarioDTO.usuario(prData.usuario);
		aUsuarioDTO.contrasena(prData.contrasena);
		aUsuarioDTO.confirmacionContrasena(prData.contrasena);
		aUsuarioDTO.nombre(prData.nombre);
		aUsuarioDTO.apellidoPaterno(prData.apellidoPaterno);
		aUsuarioDTO.apellidoMaterno(prData.apellidoMaterno);
		aUsuarioDTO.tipoDocumento(0);
		aUsuarioDTO.numeroDocumento(prData.numeroDocumento);
		aUsuarioDTO
				.fechaNacimientoDescripcion(prData.fechaNacimientoDescripcion);
		aUsuarioDTO.correo(prData.correo);
		aUsuarioDTO.telefono(prData.telefono);
		aUsuarioDTO.celular(prData.celular);
		aUsuarioDTO.creator(prData.creator);
		aUsuarioDTO.estado(prData.estado);
		aUsuarioDTO.createdDescripcion(prData.createdDescripcion);

		aListRoles([]);
		ko.utils.arrayForEach(prData.roles, function(lItem) {

			var aRolDTO = new RolDTO();
			aRolDTO.rolId(lItem.rolId);
			aRolDTO.name(lItem.name);
			aRolDTO.descripcion(lItem.descripcion);
			aRolDTO.creator(lItem.creator);
			aRolDTO.estado(lItem.estado);
			aRolDTO.createdDescripcion(lItem.createdDescripcion)
			aListRoles.push(aRolDTO);
		});
	};

	return {
		config : config,
		configView : configView,
		bto_addUser : bto_addUser,
		bto_rewardClick : bto_rewardClick,
		setUsuario : setUsuario,
		bto_clearFields : bto_clearFields,
		bto_addRol: bto_addRol,
		aUsuarioDTO : aUsuarioDTO,
		bto_updateUser : bto_updateUser,
		bto_addRol : bto_addRol,
		bto_rewardClickToUser : bto_rewardClickToUser,
		aListRoles : aListRoles,
		aRolDTO: aRolDTO,
		bto_addRolToUser: bto_addRolToUser,
		bto_removeRolClick: bto_removeRolClick
	}
});