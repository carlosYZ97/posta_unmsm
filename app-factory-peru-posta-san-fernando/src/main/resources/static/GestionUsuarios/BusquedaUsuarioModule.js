var BusquedaUsuarioModule = (function () {
	var obj = this;
	globalFunctions.extendKORequiredString(ko);
    globalFunctions.configTools(ko);
    
    var config = function (prSearchUsuarios) {
        ko.applyBindings(obj, document.getElementById(prSearchUsuarios));
        $("").showDiv({ section: prSearchUsuarios, divId: 'UsersSearchMain' });
        //configAutoComplete();
    };
    
    
    var KDStatus = {
    		Inactivo: 0,
    		Activo: 1
    }
    
    var aBusquedaUsuarioEntityCollection =  ko.observableArray([]);
    
    var FilterUsuarioDTO = function(){
    	
    	var _estado = ko.observable(-1);
    	var _nombre = ko.observable("");
    	var _apellidoPaterno = ko.observable("");
    	var _apellidoMaterno = ko.observable("");
    	var _numeroDocumento = ko.observable("");
    	var _usuario = ko.observable("");
    	
    	return{
    		estado: _estado,
    		nombre: _nombre,
    		apellidoPaterno: _apellidoPaterno,
    		apellidoMaterno: _apellidoMaterno,
    		numeroDocumento: _numeroDocumento,
    		usuario: _usuario
    	}
    }
    
    var aFilterUsuarioDTO = new FilterUsuarioDTO();
    
    var bto_searchUsers = function (){
    	searchUsers();
    }
    
    var bto_newUser = function (){
    	newUser();
    }
    
    var bto_limpiar = function(){
    	clearFields();
    }
    
    var bto_showUsuario = function(prIndex, data){
    	ShowUsuario(prIndex, data);
    }
    
    var btn_ActivateDesactivate_Click = function (prIndex, data) { activateOrDesactivateUser(prIndex, data); }
    
    var ShowUsuario = function (prIndex, prData) {

        var onSucessLoad = function() {
            $("").showDiv({ section: 'secBusquedaUsuario', divId: 'divCallUser' });
            aUsuarioModule.configView("divUsuarioMain");
            aUsuarioModule.setUsuario(prData, prIndex);

        };

        $("").coLoadDiv({
            action: "/user/userView",
            cbhSucess: onSucessLoad,
            splashscreen: true,
            elementID: "#divCallUser"
        }).callService();
    };
    
    var searchUsers = function (){
    	
    	var cbhSucess = function (data) {
    		aBusquedaUsuarioEntityCollection([]);
    		aBusquedaUsuarioEntityCollection(data);
        }

        var cbhError = function (data) {
            $.growl.error(null, 'Error', data.astMessage);
        }

        var lParameter = "prUsuarioDTO="+ko.mapping.toJSON(aFilterUsuarioDTO);
        
    	 $("").coserver({
             action: "/user/userList",
             splashscreen: true,
             cbhSucess: cbhSucess,
             cbhError: cbhError
         }).callService(lParameter);
    }
    
    var newUser = function(){
    	
    	var onSucessLoad = function(){
             $("").showDiv({ section: 'secBusquedaUsuario', divId: 'divCallUser' });
             aUsuarioModule.configView("divUsuarioMain");
    	}
    	
    	 $("").coLoadDiv({
             action: "/user/userView",
             cbhSucess: onSucessLoad,
             splashscreen: true,
             elementID: "#divCallUser"
         }).callService();
    }
    
    var activateOrDesactivateUser = function (prIndex, data) {

    	var setSuccess = function (dataUser) {
        	searchUsers();
            $.growl.notice(null, 'Éxito', 'Usuario actualizado con éxito.');
        }

        var lParametros = "prUserId=" + data.usuarioId;
        $("").coserver({
            action: "/user/activateOrDesactivateUser", splashscreen: true, cbhSucess: setSuccess, cbhError: onErrorUsuario
        }).callService(lParametros);
    }
    
    var onErrorUsuario = function (data) {
        $.growl.error(null, 'Error', (data[0] == null ? data.astMessage : data[0].astMessage));
    };
    
    var clearFields = function(){
    	aFilterUsuarioDTO.estado(-1);
    	aFilterUsuarioDTO.usuario("");
    	aFilterUsuarioDTO.numeroDocumento("");
    	aFilterUsuarioDTO.nombre("");
    	aFilterUsuarioDTO.apellidoPaterno("");
    	aFilterUsuarioDTO.apellidoMaterno("");
    }
	
	return {
		 config: config,
		 bto_searchUsers: bto_searchUsers,
		 bto_newUser:bto_newUser,
		 KDStatus:KDStatus,
		 aBusquedaUsuarioEntityCollection:aBusquedaUsuarioEntityCollection,
		 aFilterUsuarioDTO: aFilterUsuarioDTO,
		 bto_limpiar:bto_limpiar,
		 bto_showUsuario:bto_showUsuario,
		 btn_ActivateDesactivate_Click:btn_ActivateDesactivate_Click
	}
	
});