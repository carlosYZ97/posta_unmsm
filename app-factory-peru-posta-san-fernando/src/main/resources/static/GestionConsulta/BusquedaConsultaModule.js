var BusquedaConsultaModule = (function () {
	var obj = this;
	globalFunctions.extendKORequiredString(ko);
    globalFunctions.configTools(ko);
    
    var config = function (prSearchPacientes) {
        ko.applyBindings(obj, document.getElementById(prSearchPacientes));
        $("").showDiv({ section: prSearchPacientes, divId: 'ConsultaSearchMain' });
    };
    
    var bto_newConsult = function (){
    	newConsulta();
    }
    
    var newConsulta = function(){
    	
    	var onSucessLoad = function(){
             $("").showDiv({ section: 'secBusquedaConsulta', divId: 'divCallConsulta' });
             aConsultaModule.configView("searchByDni");
    	}
    	
    	 $("").coLoadDiv({
             action: "/consulta/consultaView",
             cbhSucess: onSucessLoad,
             splashscreen: true,
             elementID: "#divCallConsulta"
         }).callService();
    }
	
    
    return {
    	config:config,
    	bto_newConsult:bto_newConsult,
    }
});