var ConsultaModule = (function () {
	
	 var obj = this;

	    globalFunctions.extendKORequiredString(ko);
	    globalFunctions.configTools(ko);
	    
	    var config = function (prPaciente) {
	        ko.applyBindings(obj, document.getElementById(prPaciente));
	    };
	    
	    var configView = function (prPaciente) {
	        $("").showDiv({ section: 'secNewConsulta', divId: prPaciente });
	    }

	    var ConsultaDTO = function(){
	    	var _consultaId = ko.observable(0);
	    	var _pacienteId = ko.observable(0);
	    	var _nombre = ko.observable("");
	    	var _apellidoPaterno = ko.observable ("");
	    	var _apellidoMaterno = ko.observable ("");
	    	var _apeNombres = ko.observable("");
	    	var _numeroDocumento = ko.observable("");
	    	var _sexo = ko.observable("");
	    	var _peso = ko.observable("");
	    	var _talla = ko.observable("");
	    	var _creator = ko.observable("");
	    	var _createdDescripcion = ko.observable("");
	    	var _fechaNacimientoDescripcion = ko.observable("");
	    	var _telefono = ko.observable("");
	    	var _edad = ko.observable("");
	    	
	    	return {
	    		pacienteId: _pacienteId,
	    		nombre: _nombre,
	    		apellidoPaterno: _apellidoPaterno,
	    		apellidoMaterno: _apellidoMaterno,
	    		numeroDocumento: _numeroDocumento,
	    		apeNombres: _apeNombres,
	    		sexo: _sexo,
	    		peso: _peso,
	    		talla: _talla,
	    		creator: _creator,
	    		createdDescripcion: _createdDescripcion,	    		
	    		fechaNacimientoDescripcion : _fechaNacimientoDescripcion,
	    		telefono:_telefono,
	    		edad:_edad
	    	}
	    }
	    
	    var aConsultaDTO = new ConsultaDTO();
	    
	    var bto_rewardClick = function(){rewardClick(); }
	    
	    var bto_clearDocument = function(){clearDocument();}
	    var bto_validateExistPaciente = function (){validateExistPaciente();}
	    
	    var bto_recetaVirtual = function (){recetaVirtual();}
	    
	    var rewardClick = function (){
	         $("").showDiv({ section: 'secBusquedaConsulta', divId: 'ConsultaSearchMain' });
	    }
	    
	    var clearDocument = function(){
	    	aConsultaDTO.numeroDocumento("");
	    }
	    
	    var validateExistPaciente = function (){
	    	var cbhSucess = function (data){
	    		aConsultaDTO.sexo(data.sexo);
	    		aConsultaDTO.telefono(data.telefono);
	    		aConsultaDTO.edad("1");
	    		aConsultaDTO.apeNombres(data.apellidoPaterno + " " + data.apellidoMaterno + " " +data.nombre);
		         $("").showDiv({ section: 'secBusquedaConsulta', divId: 'divCallConsulta' });
	             aConsultaModule.configView("CreateConsultaMain");
	    	}
	    	
	    	var cbhError = function (data) {
	             $.growl.error(null, 'Error', data.responseText);
	         }
	    	
	    	var lParameter ="prPacienteDocument="+ aConsultaDTO.numeroDocumento();
	    	
	    	 $("").coserver({
	             action: "/patient/patientById",
	             splashscreen: true,
	             cbhSucess: cbhSucess,
	             cbhError: cbhError,
	         }).callService(lParameter);
	    }
	    
	    var recetaVirtual = function(){
	    	
	    	var onSucessLoad = function(){
	             $("").showDiv({ section: 'secBusquedaConsulta', divId: 'divCallConsulta' });
	             aConsultaModule.configView("divPrescripcion");
	    	}
	    	
	    	 $("").coLoadDiv({
	             action: "/admin/prescripcion",
	             cbhSucess: onSucessLoad,
	             splashscreen: true,
	             elementID: "#divCallConsulta"
	         }).callService();
	    }
	    
	return {
		config: config,
    	configView: configView,
    	bto_rewardClick:bto_rewardClick,
    	aConsultaDTO:aConsultaDTO,
    	bto_clearDocument:bto_clearDocument,
    	bto_validateExistPaciente:bto_validateExistPaciente,
    	bto_recetaVirtual: bto_recetaVirtual
	}
});