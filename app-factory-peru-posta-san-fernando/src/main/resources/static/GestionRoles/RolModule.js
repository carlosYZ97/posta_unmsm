var RolModule = (function() {
	var obj = this;

	globalFunctions.extendKORequiredString(ko);
	globalFunctions.configTools(ko);

	var config = function(prRol) {
		ko.applyBindings(obj, document.getElementById(prRol));
		configAutoComplete();
	};

	var configView = function(prRol) {
		$("").showDiv({
			section : 'secRol',
			divId : prRol
		});
	}
	
	 var configAutoComplete = function () {
		 acoacPrivilegio = $("#coacPrivilegio").coac({ action: "/search/getPrivilegiosRol", elemselected: autoCompletePrivilegio, elemremoved: elemremovedPrivilegio, width: "100%" });
	 }
	
	 var autoCompletePrivilegio = function(id, value, image){
		 aPrivilegioDTO.nombre(value);
		 aPrivilegioDTO.privilegioId(id);
		 acoacPrivilegio.ItemSelected(id,value,image);
	 }
	 
	 var elemremovedPrivilegio = function(){
		 aPrivilegioDTO.nombre("");
		 aPrivilegioDTO.privilegioId(0);
		 acoacPrivilegio.RemoveSelected();
	 }
	 
	var aListPrivilegio = ko.observableArray([]);

	var RolDTO = function() {

		var _rolId = ko.observable(0);
		var _name = ko.observable("");
		var _descripcion = ko.observable("");
		var _estado = ko.observable(1);
		var _creator = ko.observable("");
		var _createdDescripcion = ko.observable("");

		return {
			rolId: _rolId,
			name: _name,
			descripcion: _descripcion,
			creator : _creator,
			estado : _estado,
			createdDescripcion : _createdDescripcion
		}
	}
	
	var PrivilegioDTO = function() {
		var _privilegioId = ko.observable(0);
		var _nombre = ko.observable("");
		var _descripcion = ko.observable("");
		var _estado = ko.observable(1);

		return {
			privilegioId : _privilegioId,
			nombre : _nombre,
			descripcion : _descripcion,
			estado : _estado
		}
	}

	var bto_addRol = function() {
		addRol();
	}

	var bto_rewardClick = function() {
		rewardClick();
	}

	var bto_clearFields = function() {
		clearFields();
	}
	
	var bto_updateRol = function(){
		updateRol();
	}
	
	var bto_addPrivilegio = function(){
		addPrivilegio();
	}
	
	var bto_rewardClickToRol = function(){
		rewardClickToRol();
	}
	
	var bto_addPrivilegioToRol = function(){
		addPrivilegioToRol();
	}
	
	var bto_removePrivilegioClick = function(){
		removePrivilegioClick();
	}

	var aRolDTO = new RolDTO();
	var aPrivilegioDTO = new PrivilegioDTO();

	var addRol = function() {

		var cbhSucess = function(data) {
			$.growl.notice(null, 'Éxito',
					'El rol fue registrado con éxito.');

			rewardClick();
		}

		var cbhError = function(data) {
			$.growl.error(null, 'Error', data.responseText);
		}

		var lParameter = "prRolDTO=" + ko.mapping.toJSON(aRolDTO);

		$("").coserver({
			action : "/rol/rolAdd",
			splashscreen : true,
			cbhSucess : cbhSucess,
			cbhError : cbhError,
		}).callService(lParameter);
	}

	var updateRol = function() {

		var cbhSucess = function(data) {
			$.growl.notice(null, 'Éxito',
					'El rol fue actualizado con éxito.');

			rewardClick();
		}

		var cbhError = function(data) {
			$.growl.error(null, 'Error', data.responseText);
		}

		var lParameter = "prRolDTO=" + ko.mapping.toJSON(aRolDTO);

		$("").coserver({
			action : "/rol/rolAdd",
			splashscreen : true,
			cbhSucess : cbhSucess,
			cbhError : cbhError,
		}).callService(lParameter);
	}

	var rewardClick = function() {
		$("").showDiv({
			section : 'secBusquedaRol',
			divId : 'RolesSearchMain'
		});
	}
	
	var addPrivilegio = function(){
		$("").showDiv({
			section : 'secRol',
			divId : 'PrivilegioRolMain'
		});
	}
	
	var rewardClickToRol = function(){
		$("").showDiv({
			section : 'secRol',
			divId : 'divRolMain'
		});
	}
	
	var addPrivilegioToRol = function(){
		
	}
	
	var removePrivilegioClick = function(){
		
	}

	var clearFields = function() {
		aRolDTO.rolId(0);
		aRolDTO.name("");
		aRolDTO.descripcion("");
		aRolDTO.creator("");
		aRolDTO.createdDescripcion("");
	}

	var setRol = function(prData, prIndex) {
		aRolDTO.rolId(prData.rolId);
		aRolDTO.name(prData.name);
		aRolDTO.descripcion(prData.descripcion);
		aRolDTO.creator(prData.creator);
		aRolDTO.estado(prData.estado);
		aRolDTO.createdDescripcion(prData.createdDescripcion);
		
		aListPrivilegio([]);
		ko.utils.arrayForEach(prData.privilegios, function(lItem) {

			var aPrivilegioDTO = new PrivilegioDTO();
			aPrivilegioDTO.privilegioId(lItem.privilegioId);
			aPrivilegioDTO.nombre(lItem.nombre);
			aPrivilegioDTO.descripcion(lItem.descripcion);
			aPrivilegioDTO.estado(lItem.estado);
			aListPrivilegio.push(aPrivilegioDTO);
		});
	};

	return {
		config : config,
		configView : configView,
		bto_addRol : bto_addRol,
		bto_rewardClick : bto_rewardClick,
		setRol : setRol,
		bto_clearFields : bto_clearFields,

		aRolDTO : aRolDTO,
		bto_updateRol: bto_updateRol,
		aListPrivilegio: aListPrivilegio,
		bto_addPrivilegio: bto_addPrivilegio,
		bto_rewardClickToRol: bto_rewardClickToRol,
		bto_addPrivilegioToRol: bto_addPrivilegioToRol,
		bto_removePrivilegioClick: bto_removePrivilegioClick
	}
});