var BusquedaPacienteModule = (function () {
	var obj = this;
	globalFunctions.extendKORequiredString(ko);
    globalFunctions.configTools(ko);
    
    var config = function (prSearchPacientes) {
        ko.applyBindings(obj, document.getElementById(prSearchPacientes));
        $("").showDiv({ section: prSearchPacientes, divId: 'PatientsSearchMain' });
        bto_searchPatients();
    };
    
    var KDStatus = {
    		Inactivo: 0,
    		Activo: 1
    }
    
    var aBusquedaPacienteEntityCollection =  ko.observableArray([]);
    
var FilterPacienteDTO = function(){
    	
    	var _estado = ko.observable(-1);
    	var _nombre = ko.observable("");
    	var _apellidoPaterno = ko.observable("");
    	var _apellidoMaterno = ko.observable("");
    	var _numeroDocumento = ko.observable("");
    	
    	return{
    		estado: _estado,
    		nombre: _nombre,
    		apellidoPaterno: _apellidoPaterno,
    		apellidoMaterno: _apellidoMaterno,
    		numeroDocumento: _numeroDocumento
    	}
    }
    
    var aFilterPacienteDTO = new FilterPacienteDTO();
    
    var bto_searchPatients = function (){
    	searchPatients();
    }
    
    var bto_newPatient = function (){
    	newPatient();
    }
    
    var bto_limpiar = function(){
    	clearFields();
    }
    
    var bto_showPaciente = function(prIndex, data){
    	ShowPaciente(prIndex, data);
    }
    
    var btn_ActivateDesactivate_Click = function (prIndex, data) { activateOrDesactivatePatient(prIndex, data); }
    
    var ShowPaciente = function (prIndex, prData) {

        var onSucessLoad = function() {
            $("").showDiv({ section: 'secBusquedaPaciente', divId: 'divCallPatient' });
            aPacienteModule.configView("divPacienteMain");
            aPacienteModule.setPaciente(prData, prIndex);

        };

        $("").coLoadDiv({
            action: "/patient/patientView",
            cbhSucess: onSucessLoad,
            splashscreen: true,
            elementID: "#divCallPatient"
        }).callService();
    };
    
    var searchPatients = function (){
    	
    	var cbhSucess = function (data) {
    		aBusquedaPacienteEntityCollection([]);
    		aBusquedaPacienteEntityCollection(data);
        }

        var cbhError = function (data) {
            $.growl.error(null, 'Error', data.astMessage);
        }
    	
        var lParameter = "prPacienteDTO="+ko.mapping.toJSON(aFilterPacienteDTO);
        
    	 $("").coserver({
             action: "/patient/patientList",
             splashscreen: true,
             cbhSucess: cbhSucess,
             cbhError: cbhError
         }).callService(lParameter);
    }
    
    var newPatient = function(){
    	
    	var onSucessLoad = function(){
             $("").showDiv({ section: 'secBusquedaPaciente', divId: 'divCallPatient' });
             aPacienteModule.configView("divPacienteMain");
    	}
    	
    	 $("").coLoadDiv({
             action: "/patient/patientView",
             cbhSucess: onSucessLoad,
             splashscreen: true,
             elementID: "#divCallPatient"
         }).callService();
    }
	
    var activateOrDesactivatePatient = function (prIndex, data) {

    	var setSuccess = function (dataPatient) {
    		searchPatients();
            $.growl.notice(null, 'Éxito', 'Paciente actualizado con éxito.');
        }

        var lParametros = "prPatientId=" + data.pacienteId;
        $("").coserver({
            action: "/patient/activateOrDesactivatePatient", splashscreen: true, cbhSucess: setSuccess, cbhError: onErrorPaciente
        }).callService(lParametros);
    }
    
    var onErrorPaciente = function (data) {
        $.growl.error(null, 'Error', (data[0] == null ? data.astMessage : data[0].astMessage));
    };
    
    var clearFields = function(){
    	aFilterPacienteDTO.estado(-1);
    	aFilterPacienteDTO.numeroDocumento("");
    	aFilterPacienteDTO.nombre("");
    	aFilterPacienteDTO.apellidoPaterno("");
    	aFilterPacienteDTO.apellidoMaterno("");
    }
    
	return {
		 config: config,
		 bto_searchPatients: bto_searchPatients,
		 bto_newPatient: bto_newPatient,
		 KDStatus:KDStatus,
		 aBusquedaPacienteEntityCollection: aBusquedaPacienteEntityCollection,
		 aFilterPacienteDTO: aFilterPacienteDTO,
		 bto_limpiar:bto_limpiar,
		 bto_showPaciente:bto_showPaciente,
		 btn_ActivateDesactivate_Click:btn_ActivateDesactivate_Click
	}
	
});