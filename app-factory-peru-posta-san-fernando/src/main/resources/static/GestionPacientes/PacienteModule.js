var PacienteModule = (function () {
    var obj = this;

    globalFunctions.extendKORequiredString(ko);
    globalFunctions.configTools(ko);
    
    var config = function (prPaciente) {
        ko.applyBindings(obj, document.getElementById(prPaciente));
    };
    
    var configView = function (prPaciente) {
        $("").showDiv({ section: 'secPaciente', divId: prPaciente });
        loadRegion(KDRegion.Departament, 0);
    }
    
    var RegionDTO = function () {
        var _DEPARTAMENTO = ko.observable(0);
        var _PROVINCIA = ko.observable(0);
        var _DISTRITO = ko.observable(0);
        return {
            departamento: _DEPARTAMENTO,
            provincia: _PROVINCIA,
            distrito: _DISTRITO
        }
    };
    
    var aListDepartamentoViewModel = ko.observableArray([]);
    var aListProvinciaViewModel = ko.observableArray([]);
    var aListDistritoViewModel = ko.observableArray([]);

    var KDRegion = {
        Departament: 0,
        Provincia: 1,
        District: 2
    }
    
    var PacienteDTO = function(){
    	
    	var _pacienteId = ko.observable(0);
    	var _nombre = ko.observable("");
    	var _apellidoPaterno = ko.observable ("");
    	var _apellidoMaterno = ko.observable ("");
    	var _numeroDocumento = ko.observable("");
    	var _sexo = ko.observable("");
    	var _estadoCivil = ko.observable("");
    	var _lugarNacimiento = ko.observable("");
    	var _peso = ko.observable("");
    	var _talla = ko.observable("");
    	var _direccionActual = ko.observable("");
    	var _regionId = ko.observable("");
    	var _telefono = ko.observable("");
    	var _celular = ko.observable("");
    	var _correo = ko.observable("");
    	var _nombreContacto = ko.observable("");
    	var _telefonoContacto = ko.observable("");
    	var _correoContacto = ko.observable("");
    	var _estado = ko.observable(1);
    	var _creator = ko.observable("");
    	var _createdDescripcion = ko.observable("");
    	var _raza = ko.observable("");
    	var _religion = ko.observable("");
    	var _nacionalidad = ko.observable("");
    	var _ocupacion = ko.observable("");
    	var _gradoInstruccion = ko.observable(-1);
    	var _fechaNacimientoDescripcion = ko.observable("");
    	
    	return {
    		pacienteId: _pacienteId,
    		nombre: _nombre,
    		apellidoPaterno: _apellidoPaterno,
    		apellidoMaterno: _apellidoMaterno,
    		numeroDocumento: _numeroDocumento,
    		sexo: _sexo,
    		estadoCivil: _estadoCivil,
    		lugarNacimiento: _lugarNacimiento,
    		peso: _peso,
    		talla: _talla,
    		direccionActual: _direccionActual,
    		regionId: _regionId,
    		telefono: _telefono,
    		celular: _celular,
    		correo: _correo,
    		nombreContacto: _nombreContacto,
    		telefonoContacto: _telefonoContacto,
    		correoContacto: _correoContacto,
    		ocupacion : _ocupacion,
    		gradoInstruccion : _gradoInstruccion,
    		raza : _raza,
    		religion : _religion,
    		nacionalidad : _nacionalidad,
    		estado: _estado,
    		creator: _creator,
    		createdDescripcion: _createdDescripcion,	    		
    		fechaNacimientoDescripcion : _fechaNacimientoDescripcion
    	}
    }
    
    var eventGetProvincia = function () {
        cleanRegion();
        if (aRegion.departamento() > 0) {
            $("#edProvincia").attr("disabled", false);
            loadRegion(KDRegion.Provincia, aRegion.departamento());
        }
    };

    var eventGetDistrito = function () {
        if (aRegion.provincia() > 0) {
            $("#edDistrito").attr("disabled", false);
            loadRegion(KDRegion.District, aRegion.provincia());
        }
    };
    
    var bto_addPatient = function(){addPatient();}
    
    var bto_rewardClick = function(){rewardClick(); }
    
    var aPacienteDTO = new PacienteDTO();
    var aRegion = new RegionDTO();
    
    var bto_clearFields = function() {
		clearFields();
	}
    
    var bto_updatePatient = function(){
		updatePatient();
	}
    
    var addPatient = function (){
    	aPacienteDTO.regionId(aRegion.distrito());
    	var cbhSucess = function (data){
    		 $.growl.notice(null, 'Éxito', 'El paciente fue registrado con éxito.');	 
    		 rewardClick();
    	}
    	
    	var cbhError = function (data) {
             $.growl.error(null, 'Error', data.astMessage);
         }
    	
    	var lParameter ="prPacienteDTO="+ko.mapping.toJSON(aPacienteDTO);
    	
    	 $("").coserver({
             action: "/patient/patientAdd",
             splashscreen: true,
             cbhSucess: cbhSucess,
             cbhError: cbhError,
         }).callService(lParameter);
    }
    
    var updatePatient = function() {
    	aPacienteDTO.regionId(aRegion.distrito());
		var cbhSucess = function(data) {
			$.growl.notice(null, 'Éxito',
					'El paciente fue actualizado con éxito.');

			rewardClick();
		}

		var cbhError = function(data) {
			$.growl.error(null, 'Error', data.astMessage);
		}

		var lParameter = "prPacienteDTO=" + ko.mapping.toJSON(aPacienteDTO);

		$("").coserver({
			action : "/patient/patientAdd",
			splashscreen : true,
			cbhSucess : cbhSucess,
			cbhError : cbhError,
		}).callService(lParameter);
	}
    
    var loadRegion = function (RegionType, RegionID) {
        var setSucess = function (data) {
            if (RegionType == KDRegion.Departament) aListDepartamentoViewModel(data);

            if (RegionType == KDRegion.Provincia) aListProvinciaViewModel(data);

            if (RegionType == KDRegion.District) aListDistritoViewModel(data);
        }

        var onError = function (data) {
            $.growl.error(null, 'Error', data.responseText);
        }

        var lParametros = 'prRegionType=' + RegionType;
        lParametros += '&prRegionID=' + RegionID;

        $("").coserver({
            action: "/patient/regionByFilter",
            cbhSucess: setSucess,
            cbhError: onError,
            splashscreen: true
        }).callService(lParametros);        
    }
    
    var rewardClick = function (){
         $("").showDiv({ section: 'secBusquedaPaciente', divId: 'PatientsSearchMain' });
    }
    
    var cleanRegion = function () {
        aRegion.provincia(0);
        aRegion.distrito(0);
        aListProvinciaViewModel([]);
        aListDistritoViewModel([]);
        $("#edProvincia").attr("disabled", true);
        $("#edDistrito").attr("disabled", true);
    }
    
    var clearFields = function() {
    	aPacienteDTO.pacienteId(0);
    	aPacienteDTO.nombre("");
    	aPacienteDTO.apellidoPaterno("");
    	aPacienteDTO.apellidoMaterno("");
    	aPacienteDTO.numeroDocumento("");
    	aPacienteDTO.sexo(-1);
    	aPacienteDTO.estadoCivil(-1);
    	aPacienteDTO.lugarNacimiento("");
    	aPacienteDTO.peso("");
    	aPacienteDTO.talla("");
    	aPacienteDTO.direccionActual("");
    	aPacienteDTO.regionId(-1);    	
    	aPacienteDTO.telefono("");
    	aPacienteDTO.celular("");
    	aPacienteDTO.correo("");
    	aPacienteDTO.nombreContacto("");
    	aPacienteDTO.telefonoContacto("");
    	aPacienteDTO.correoContacto("");
    	aPacienteDTO.ocupacion("");
    	aPacienteDTO.gradoInstruccion(-1);
    	aPacienteDTO.raza("");
    	aPacienteDTO.religion("");
    	aPacienteDTO.nacionalidad("");
    	aPacienteDTO.creator("");
    	aPacienteDTO.createdDescripcion("");
    	aPacienteDTO.fechaNacimientoDescripcion("");
	}
    
    var setPaciente = function(prData, prIndex) {
    	aPacienteDTO.pacienteId(prData.pacienteId);    	
    	aPacienteDTO.nombre(prData.nombre);
    	aPacienteDTO.apellidoPaterno(prData.apellidoPaterno);
    	aPacienteDTO.apellidoMaterno(prData.apellidoMaterno);	
    	aPacienteDTO.numeroDocumento(prData.numeroDocumento);
    	aPacienteDTO.sexo(prData.sexo);
    	aPacienteDTO.estadoCivil(prData.estadoCivil);
    	aPacienteDTO.lugarNacimiento(prData.lugarNacimiento);
    	aPacienteDTO.peso(prData.peso);
    	aPacienteDTO.talla(prData.talla);
    	aPacienteDTO.direccionActual(prData.direccionActual);
    	aPacienteDTO.regionId(prData.regionId);    	
    	aPacienteDTO.telefono(prData.telefono);
    	aPacienteDTO.celular(prData.celular);
    	aPacienteDTO.correo(prData.correo);
    	aPacienteDTO.nombreContacto(prData.nombreContacto);
    	aPacienteDTO.telefonoContacto(prData.telefonoContacto);
    	aPacienteDTO.correoContacto(prData.correoContacto);
    	aPacienteDTO.ocupacion(prData.ocupacion);
    	aPacienteDTO.gradoInstruccion(prData.gradoInstruccion);
    	aPacienteDTO.raza(prData.raza);
    	aPacienteDTO.religion(prData.religion);
    	aPacienteDTO.nacionalidad(prData.nacionalidad);
    	aPacienteDTO.creator(prData.creator);
    	aPacienteDTO.estado(prData.estado);
    	aPacienteDTO.createdDescripcion(prData.createdDescripcion);
    	aPacienteDTO.fechaNacimientoDescripcion(prData.fechaNacimientoDescripcion);
	};
    
    return {
    	config: config,
    	configView: configView,
    	bto_addPatient: bto_addPatient,
    	bto_rewardClick:bto_rewardClick,
    	setPaciente: setPaciente,
    	bto_clearFields : bto_clearFields,
    	cleanRegion : cleanRegion,
    	
    	aRegion: aRegion,
    	aPacienteDTO: aPacienteDTO,
    	bto_updatePatient:bto_updatePatient,
    	loadRegion: loadRegion,
    	aListDepartamentoViewModel:aListDepartamentoViewModel,
    	aListProvinciaViewModel: aListProvinciaViewModel,
    	aListDistritoViewModel:aListDistritoViewModel,
    	eventGetProvincia: eventGetProvincia,
    	eventGetDistrito: eventGetDistrito
    }
});