﻿var paginationModule = function () {

    var _CollectionAll = undefined;
    var _CollectionGrid = ko.observableArray([]);

    var _pageNumber = 0;
    var _nbPerPage = 5;

    var _hasPrevious = ko.observable(false);
    var _hasNext = ko.observable(false);

    var setListAll = function (prCollection){
        _CollectionAll = prCollection;
        _CollectionGrid(getPaginated());
    };

    var getPaginated = function () { };

    var _next = function () {
        if (_pageNumber < totalPages()) {
            _pageNumber = _pageNumber + 1;
            _CollectionGrid(getPaginated());
        }
        $('[data-toggle="tooltip"]').tooltip();
    };
    var _previous = function () {
        if (_pageNumber != 0) {
            _pageNumber = _pageNumber - 1;
            _CollectionGrid(getPaginated());
        }
        $('[data-toggle="tooltip"]').tooltip();
    };

    var getPaginated = function () {
        var first = _pageNumber * _nbPerPage;

        _hasNext(_pageNumber !== totalPages());
        _hasPrevious(_pageNumber !== 0);

        return _CollectionAll.slice(first, first + _nbPerPage);
    };

    var totalPages = function () {
        var div = Math.floor(_CollectionAll.length / _nbPerPage);
        div += _CollectionAll.length % _nbPerPage > 0 ? 1 : 0;
        return div - 1;
    };

    return {
        setListAll: setListAll,
        CollectionGrid : _CollectionGrid,
        hasPrevious  :_hasPrevious,
        hasNext      :_hasNext,
        next         :_next,
        previous: _previous,
        nbPerPage: _nbPerPage
    };
};


