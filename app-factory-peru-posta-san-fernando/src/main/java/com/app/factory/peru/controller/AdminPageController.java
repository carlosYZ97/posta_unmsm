package com.app.factory.peru.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AdminPageController {
	
	@RequestMapping({"/","/index","/login"})
    public String login(){
        return "admin/Login/Login";
    }
	
	@RequestMapping({"/admin/dashboard","/admin"})
    public String dashboard(){
        return "admin/dashboard";
    }

    @RequestMapping({"/	","/admin/user"})
    public String listUser(){
        return "admin/user-list";
    }

    @RequestMapping("/admin/user/add")
    public String addUser(){
        return "admin/user-add";
    }
    
    @RequestMapping("/admin/usuario")
    public String searchUser() {
    	return "admin/GestionUsuarios/BuscarGestionUsuario";
    }
    
    @RequestMapping("/user/userView")
	public String usuarioView() {
		return "admin/GestionUsuarios/NewUsuario";
	}
    
    @RequestMapping({"/admin/patient/list","/admin/patient"})
    public String listPatient(){
        return "admin/patient-list";
    }

    @RequestMapping("/admin/patient/add")
    public String addPatient(){
        return "admin/patient-add";
    }
    
    @RequestMapping("/admin/paciente")
    public String searchPatient() {
    	return  "admin/GestionPacientes/BuscarGestionPaciente";
    }
    
    @RequestMapping("/patient/patientView")
	public String pacienteView() {
		return "admin/GestionPacientes/NewPaciente";
	}
    
    @RequestMapping("admin/prescripcion")
    public String prescriptionView() {
    	return "admin/PrescripcionMedica/PrescripcionMedica";
    }

    @RequestMapping("/admin/consulta")
    public String searchConsult() {
    	return "admin/GestionConsulta/BuscarGestionConsulta";
    }
    
    @RequestMapping("/consulta/consultaView")
	public String consultaView() {
		return "admin/GestionConsulta/NewConsulta";
	}
    
//    @RequestMapping({"/admin/rol/list","/admin/rol"})
//    public String listRol(){
//        return "admin/rol-list";
//    }

    @RequestMapping("/admin/rol/add")
    public String addRol(){
        return "admin/rol-add";
    }
    
    @RequestMapping("/admin/rol")
    public String searchRol() {
    	return "admin/GestionRoles/BuscarGestionRol";
    }
    
    @RequestMapping("/rol/rolView")
	public String rolView() {
		return "admin/GestionRoles/NewRol";
	}
	
    @RequestMapping("/admin/historialClinico")
    public String historialClinicoView() {
    	return "admin/HistorialClinico/HistorialClinico";
    }
    
    @RequestMapping("/admin/tipoUsuario")
    public String searchTipoUsuario(){
    	return "admin/Configuraciones/TipoUsuario/BusquedaTipoUsuario";
    }
    
    @RequestMapping("/tipoUsuario/tipoUsuarioView")
    public String tipoUsuarioView() {
    	return "admin/Configuraciones/TipoUsuario/NewTipoUsuario";
    }
}
