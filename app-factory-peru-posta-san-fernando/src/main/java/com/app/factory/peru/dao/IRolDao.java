package com.app.factory.peru.dao;

import java.util.List;

import com.app.factory.peru.dto.RolDTO;

public interface IRolDao {
	
	public List<RolDTO> getAllRol() throws Exception;
	public RolDTO saveRol(RolDTO prRolDTO) throws Exception;
	public List<RolDTO> getByFilter(RolDTO prRolDTO) throws Exception;
	public RolDTO activateOrDesactivateRol(String prRolId) throws Exception;
	public List<RolDTO> findbyNombreRol(String keyword) throws Exception;
}
