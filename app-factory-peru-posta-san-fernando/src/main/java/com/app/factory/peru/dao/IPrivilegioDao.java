package com.app.factory.peru.dao;

import java.util.List;

import com.app.factory.peru.dto.PrivilegioDTO;

public interface IPrivilegioDao {

	public List<PrivilegioDTO> findbyNombrePrivilegio(String keyword) throws Exception;
	
}
