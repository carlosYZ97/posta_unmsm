package com.app.factory.peru.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TIPOUSUARIO")
public class TipoUsuarioEntity implements Serializable{
	private static final long serialVersionUID = -2107370678700171759L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TIPOUSUARIOID")
	private Long tipoUsuarioId;
	@Column(name = "NOMBRE")
	private String nombre;
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@Column(name = "ESTADO")
	private int estado;
	@Column(name = "CREATOR")
	private String creator;
	@Column(name = "CREATED")
	private Date created;
	@Column(name = "CHANGER")
	private String changer;
	@Column(name = "CHANGED")
	private Date changed;
	
	public Long getTipoUsuarioId() {
		return tipoUsuarioId;
	}
	public void setTipoUsuarioId(Long tipoUsuarioId) {
		this.tipoUsuarioId = tipoUsuarioId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getChanger() {
		return changer;
	}
	public void setChanger(String changer) {
		this.changer = changer;
	}
	public Date getChanged() {
		return changed;
	}
	public void setChanged(Date changed) {
		this.changed = changed;
	}
	@Override
	public String toString() {
		return "TipoUsuarioEntity [tipoUsuarioId=" + tipoUsuarioId + ", nombre=" + nombre + ", descripcion="
				+ descripcion + ", estado=" + estado + ", creator=" + creator + ", created=" + created + ", changer="
				+ changer + ", changed=" + changed + "]";
	}

}
