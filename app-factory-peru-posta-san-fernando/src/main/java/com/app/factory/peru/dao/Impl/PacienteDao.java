package com.app.factory.peru.dao.Impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.app.factory.peru.converter.PacienteConverter;
import com.app.factory.peru.dao.IPacienteDao;
import com.app.factory.peru.dao.IRegionDao;
import com.app.factory.peru.dto.PacienteDTO;
import com.app.factory.peru.dto.RegionDTO;
import com.app.factory.peru.entity.PacienteEntity;
import com.app.factory.peru.service.IPacienteService;

@Component("pacienteDao")
public class PacienteDao implements IPacienteDao{
	
	@Autowired
	@Qualifier("pacienteConverter")
	private PacienteConverter pacienteConverter;
	
	@Autowired
	@Qualifier("regionDao")
	private IRegionDao lRegionDao;
	
	@Autowired
	private IPacienteService pacienteService;
	
	@Override
	public List<PacienteDTO> getAllPatient(){
		
		List<PacienteDTO> lPacienteDTOCollection = new ArrayList<>();
		
		this.pacienteService.getAllPatient().stream().forEach( c -> {
			try {
				lPacienteDTOCollection.add( this.pacienteConverter.entityToDTO(c));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
		return lPacienteDTOCollection;
	}
	
	@Override
	public PacienteDTO savePatient(PacienteDTO prPacienteDTO) throws Exception {
		
		PacienteDTO lPacienteDTO = null;
		try {
			RegionDTO regionDTO = lRegionDao.getByRegioNumero(Integer.valueOf(prPacienteDTO.getRegionId().toString()));
			prPacienteDTO.setRegionId(regionDTO);
			
			PacienteEntity pacienteEntity = pacienteConverter.dtoToEntity(prPacienteDTO);
			
			pacienteEntity.setCreated(new Date());			
			lPacienteDTO = pacienteConverter.entityToDTO(pacienteService.add(pacienteEntity));
			
		}catch(ParseException e) {
			e.printStackTrace();
		}
		
		return lPacienteDTO;
	}
	
	@Override
	public List<PacienteDTO> getByFilter(PacienteDTO prPacienteDTO) throws Exception{
		
		List<PacienteDTO> lPacienteDTOCollection = new ArrayList<>();
			
		try {
			PacienteEntity pacienteEntity = pacienteConverter.dtoToEntity(prPacienteDTO);
			this.pacienteService.getByFilter(pacienteEntity).stream().forEach( c -> {
				try {
					lPacienteDTOCollection.add( this.pacienteConverter.entityToDTO(c));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lPacienteDTOCollection;
	}

	@Override
	public PacienteDTO activateOrDesactivatePatient(String prPatientId) throws Exception {

		java.util.Optional<PacienteEntity> optionalPacienteEntity = pacienteService.getByPatientId(prPatientId);
		PacienteDTO lPacienteDTO = null;
		if(optionalPacienteEntity.isPresent()) {
			PacienteEntity pacienteEntity = optionalPacienteEntity.get();
			pacienteEntity.setEstado((pacienteEntity.getEstado() == 0)? 1 : 0);
			pacienteEntity.setChanged(new Date());
			pacienteEntity.setChanger("cm");
			lPacienteDTO = pacienteConverter.entityToDTO(pacienteService.add(pacienteEntity));
		}
		return lPacienteDTO;
	}

	@Override
	public PacienteDTO getPacienteByDocument(String prPacienteDocument) throws Exception {
		java.util.Optional<PacienteEntity> optionalPacienteEntity = pacienteService.getByPatientDocument(prPacienteDocument);
		PacienteDTO lPacienteDTO = null;
		if(optionalPacienteEntity.isPresent()) {
			PacienteEntity pacienteEntity = optionalPacienteEntity.get();
			lPacienteDTO = pacienteConverter.entityToDTO(pacienteEntity);
		}
		return lPacienteDTO;
	}
}
