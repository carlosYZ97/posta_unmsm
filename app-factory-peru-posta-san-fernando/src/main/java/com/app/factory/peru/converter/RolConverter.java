package com.app.factory.peru.converter;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.app.factory.peru.dto.PrivilegioDTO;
import com.app.factory.peru.dto.RolDTO;
import com.app.factory.peru.entity.PrivilegioEntity;
import com.app.factory.peru.entity.RolEntity;

@Component("rolConverter")
public class RolConverter {
	
	@Autowired
	@Qualifier("privilegioConverter")
	private PrivilegioConverter lPrivilegioConverter;
	
	public RolEntity dtoToEntity(RolDTO prRolDTO) throws Exception {

		RolEntity lRolEntity = new RolEntity();
		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		lRolEntity.setRolId(prRolDTO.getRolId());
		lRolEntity.setName(prRolDTO.getName());
		lRolEntity.setDescripcion(prRolDTO.getDescripcion());
		lRolEntity.setEstado(prRolDTO.getEstado());
		
		if (prRolDTO.getCreatedDescripcion() != null && !prRolDTO.getCreatedDescripcion().isEmpty()
				&& prRolDTO.getCreated() == null) {
			lRolEntity.setCreated(lSimpleDateFormat.parse(prRolDTO.getCreatedDescripcion()));
		}
		
		lRolEntity.setCreator(prRolDTO.getCreator());

		Set<PrivilegioEntity> privilegioEntity = new HashSet<PrivilegioEntity>();
		if (prRolDTO.getPrivilegios() != null ) {
			prRolDTO.getPrivilegios().stream().forEach(c -> {
				try {
					privilegioEntity.add(lPrivilegioConverter.dtoToEntity(c));
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			lRolEntity.setPrivilegios(privilegioEntity);
		}
		
		return lRolEntity;
	}

	public RolDTO entityToDTO(RolEntity prRolEntity) {

		RolDTO lRolDTO = new RolDTO();

		lRolDTO.setRolId(prRolEntity.getRolId());
		lRolDTO.setName(prRolEntity.getName());
		lRolDTO.setDescripcion(prRolEntity.getDescripcion());
		lRolDTO.setCreator(prRolEntity.getCreator());
		lRolDTO.setEstado(prRolEntity.getEstado());
		lRolDTO.setEstadoDescripcion((prRolEntity.getEstado() == 0) ? "Inactivo" : "Activo");

		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		lRolDTO.setCreatedDescripcion(lSimpleDateFormat.format(prRolEntity.getCreated()));
		
		Set<PrivilegioDTO> privilegioDTO = new HashSet<PrivilegioDTO>();
		if (prRolEntity.getPrivilegios() != null ) {
			prRolEntity.getPrivilegios().stream().forEach(c -> {
				try {
					privilegioDTO.add(lPrivilegioConverter.entityToDTO(c));
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			lRolDTO.setPrivilegios(privilegioDTO);
		}

		return lRolDTO;
	}
}
