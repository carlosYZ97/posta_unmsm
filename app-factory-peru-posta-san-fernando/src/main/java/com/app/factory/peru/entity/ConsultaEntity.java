package com.app.factory.peru.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CONSULTA")
public class ConsultaEntity implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CONSULTAID")
	private Long consultaId;
	
//	@OneToOne
//	@JoinColumn()
//	private PacienteEntity paciente;
	@Column(name = "MENARQUIA")
	private String menarquia;
	@Column(name = "METCONCEPTIVO")
	private String metConceptivo;
	@Column(name = "REGIMENCATAMENIAL")
	private String regimenCatamenial;
	@Column(name = "ORIENTACIONSEXUAL")
	private String orientacionSexual;
	@Column(name = "FUR")
	private Date fur;
	@Column(name = "NROPAREJASEXUALES")
	private String nroParejaSexuales;
	
	private static final long serialVersionUID = -3963001362276300057L;

	
	
}
