package com.app.factory.peru.dto;

import java.util.Date;
import java.util.Set;

public class UsuarioDTO {

	private Long usuarioId;
	private String usuario;
	private String contrasena;
	private String confirmarContrasena;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String numeroDocumento;
	private Date fechaNacimiento;
	private String fechaNacimientoDescripcion;
	private String correo;
	private String telefono;
	private String celular;
	private int estado;
	private String estadoDescripcion;
	private String creator;
	private Date created;
	private String createdDescripcion;
	private String changer;
	private Date changed;
	private String changedDescripcion;
	private Set<RolDTO> roles;
	
	public UsuarioDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getConfirmarContrasena() {
		return confirmarContrasena;
	}

	public void setConfirmarContrasena(String confirmarContrasena) {
		this.confirmarContrasena = confirmarContrasena;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFechaNacimientoDescripcion() {
		return fechaNacimientoDescripcion;
	}

	public void setFechaNacimientoDescripcion(String fechaNacimientoDescripcion) {
		this.fechaNacimientoDescripcion = fechaNacimientoDescripcion;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getEstadoDescripcion() {
		return estadoDescripcion;
	}

	public void setEstadoDescripcion(String estadoDescripcion) {
		this.estadoDescripcion = estadoDescripcion;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getCreatedDescripcion() {
		return createdDescripcion;
	}

	public void setCreatedDescripcion(String createdDescripcion) {
		this.createdDescripcion = createdDescripcion;
	}

	public String getChanger() {
		return changer;
	}

	public void setChanger(String changer) {
		this.changer = changer;
	}

	public Date getChanged() {
		return changed;
	}

	public void setChanged(Date changed) {
		this.changed = changed;
	}

	public String getChangedDescripcion() {
		return changedDescripcion;
	}

	public void setChangedDescripcion(String changedDescripcion) {
		this.changedDescripcion = changedDescripcion;
	}
	
	public Set<RolDTO> getRoles() {
		return roles;
	}

	public void setRoles(Set<RolDTO> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "UsuarioDTO [usuarioId=" + usuarioId + ", usuario=" + usuario + ", contrasena=" + contrasena
				+ ", nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno
				+ ", numeroDocumento=" + numeroDocumento + ", fechaNacimiento=" + fechaNacimiento
				+ ", fechaNacimientoDescripcion=" + fechaNacimientoDescripcion + ", correo=" + correo + ", telefono="
				+ telefono + ", celular=" + celular + ", estado=" + estado + ", estadoDescripcion=" + estadoDescripcion
				+ ", creator=" + creator + ", created=" + created + ", createdDescripcion=" + createdDescripcion
				+ ", changer=" + changer + ", changed=" + changed + ", changedDescripcion=" + changedDescripcion + "]";
	}
	
}
