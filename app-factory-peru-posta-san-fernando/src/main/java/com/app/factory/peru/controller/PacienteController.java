package com.app.factory.peru.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.IPacienteDao;
import com.app.factory.peru.dao.IRegionDao;
import com.app.factory.peru.dto.PacienteDTO;
import com.app.factory.peru.dto.RegionDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class PacienteController {
	
	private static final Logger LOGGER = LogManager.getLogger(PacienteController.class);
	
	@Autowired
	@Qualifier("pacienteDao")
	private IPacienteDao lPacienteDao;
	
	@Autowired
	@Qualifier("regionDao")
	private IRegionDao lRegionDao;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@PostMapping("/patient/patientList")
	public List<PacienteDTO> getPatientList(String prPacienteDTO) throws Exception{
		
		PacienteDTO lPacienteDTO = null;
		
		try {
			lPacienteDTO = objectMapper.readValue(prPacienteDTO, PacienteDTO.class);
			
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return lPacienteDao.getByFilter(lPacienteDTO);
	}
	
	@PostMapping("/patient/patientAdd")
	public PacienteDTO getPatientAdd(String prPacienteDTO) {
		
		PacienteDTO lPacienteDTO = null;
		
		try {
			 lPacienteDTO = objectMapper.readValue(prPacienteDTO, PacienteDTO.class);
			
			lPacienteDTO.setCreator("edsan");
			
			lPacienteDao.savePatient(lPacienteDTO);
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lPacienteDTO;
	}
	
	@PostMapping("/patient/activateOrDesactivatePatient")
	public PacienteDTO activateOrDesactivatePatient(String prPatientId) throws Exception {
		
		return lPacienteDao.activateOrDesactivatePatient(prPatientId);
	}
	
	@PostMapping("/patient/patientById")
	public ResponseEntity<?> getPacienteByDocument(String prPacienteDocument) {
		
		PacienteDTO lPacienteDTO = null;
		
		LOGGER.warn("***** Inició getPacienteByDocument *****");
		
		LOGGER.warn("El número de Documento es : " + prPacienteDocument);
		
		try {
			
			lPacienteDTO = lPacienteDao.getPacienteByDocument(prPacienteDocument);
			
			if(lPacienteDTO != null) {
				return new ResponseEntity<PacienteDTO>(lPacienteDTO,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("El paciente con DNI " + prPacienteDocument +" no existe",HttpStatus.BAD_REQUEST);
			}
			
		}catch (Exception e) {
			LOGGER.error("Error getPacienteByDocument: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener los datos del Paciente, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}finally {
			
			LOGGER.warn("***** Fin getPacienteByDocument *****");
		}
		
	}
	
	@PostMapping("/patient/regionByFilter")
	public ResponseEntity<?> getRegionByFilter(String prRegionType, String prRegionID) {
		List<RegionDTO> lRegionDTOCollection = new ArrayList<>();
		try {
			lRegionDTOCollection = lRegionDao.getByFilter(prRegionType,prRegionID);
			
			if(lRegionDTOCollection.size() > 0) {
				return new ResponseEntity<List<RegionDTO>>(lRegionDTOCollection,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
						
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error getRolList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Roles, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
}
