package com.app.factory.peru.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.factory.peru.entity.UsuarioEntity;

@Repository
public interface IUsuarioRepository extends CrudRepository<UsuarioEntity, Long>{
	
	@Query("FROM UsuarioEntity WHERE usuario = IFNULL(?1,usuario) AND nombre = IFNULL(?2,nombre) AND apellidoPaterno = IFNULL(?3,apellidoPaterno)"
			+ "AND apellidoMaterno = IFNULL(?4,apellidoMaterno) AND numeroDocumento = IFNULL(?5,numeroDocumento) AND estado = IFNULL(?6,estado)")
	List<UsuarioEntity> findbyFilter(String usuario,String nombre, String apePat,String apeMat, String nroDocumento, String estado);

	Optional<UsuarioEntity> findByUsuario(String username);

	Optional<UsuarioEntity> findByUsuarioAndEstado(String username, int i);
}
