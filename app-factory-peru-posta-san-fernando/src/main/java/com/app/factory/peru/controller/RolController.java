package com.app.factory.peru.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.factory.peru.dao.IRolDao;
import com.app.factory.peru.dto.RolDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class RolController {
	
	private static final Logger LOGGER = LogManager.getLogger(RolController.class);
	
	@Autowired
	@Qualifier("rolDao")
	private IRolDao lRolDao;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@PostMapping("/rol/rolList")
	public ResponseEntity<?> getRolList(String prRolDTO){
		
		RolDTO lRolDTO = null;
		
		try {
			lRolDTO = objectMapper.readValue(prRolDTO, RolDTO.class);
			List<RolDTO> lRolDTOCollection = lRolDao.getByFilter(lRolDTO);
			
			if(lRolDTOCollection.size() > 0) {
				return new ResponseEntity<List<RolDTO>>(lRolDTOCollection,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
			
		} catch (Exception e) {
			LOGGER.error("Error getRolList: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Roles, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/rol/rolAdd")
	public ResponseEntity<?> saveRol(String prRolDTO) {
		
		RolDTO lRolDTO = null;
		
		try {
			lRolDTO = objectMapper.readValue(prRolDTO, RolDTO.class);
			
			lRolDTO = lRolDao.saveRol(lRolDTO);
		
			if(lRolDTO != null) {
				return new ResponseEntity<RolDTO>(lRolDTO,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("Se tuvo un error al guardar los datos del Rol, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error saveRol: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al guardar los datos del Rol, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@PostMapping("/rol/activateOrDesactivateRol")
	public ResponseEntity<?> activateOrDesactivateRol(String prRolId) {
		try {
			RolDTO lRolDTO = lRolDao.activateOrDesactivateRol(prRolId);
			if(lRolDTO != null) {
				return new ResponseEntity<RolDTO>(lRolDTO,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("Se tuvo un error al actualizar los datos del Rol, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("Error activateOrDesactivateRol: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar los datos del Rol, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
}
