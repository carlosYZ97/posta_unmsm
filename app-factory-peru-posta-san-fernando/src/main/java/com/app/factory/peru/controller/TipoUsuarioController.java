package com.app.factory.peru.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import com.app.factory.peru.dao.ITipoUsuarioDao;
import com.app.factory.peru.dto.TipoUsuarioDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class TipoUsuarioController {

	private static final Logger LOGGER = LogManager.getLogger(TipoUsuarioController.class);
	
	@Autowired
	@Qualifier("tipoUsuarioDao")
	private ITipoUsuarioDao lTipoUsuarioDao;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@PostMapping("/tipoUsuario/tipoUsuarioList")
	public ResponseEntity<?> getTipoUsuarioList(String prTipoUsuarioDTO){
		
		TipoUsuarioDTO lTipoUsuarioDTO = null;
		LOGGER.warn("TipoUsuario : " + prTipoUsuarioDTO);
		try {
			lTipoUsuarioDTO = objectMapper.readValue(prTipoUsuarioDTO, TipoUsuarioDTO.class);
			List<TipoUsuarioDTO> lTipoUsuarioDTOCollection = lTipoUsuarioDao.getByFilter(lTipoUsuarioDTO);
			
			if(lTipoUsuarioDTOCollection.size() > 0) {
				return new ResponseEntity<List<TipoUsuarioDTO>>(lTipoUsuarioDTOCollection,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("No se encontraron resultados.",HttpStatus.BAD_REQUEST);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error getTipoUsuarioList: detalle: " + e.getLocalizedMessage());
			return new ResponseEntity<String>("Se tuvo un error al obtener la lista de Tipos de Usuarios, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/tipoUsuario/tipoUsuarioAdd")
	public ResponseEntity<?> saveTipoUsuario(String prTipoUsuarioDTO) {
		
		TipoUsuarioDTO lTipoUsuarioDTO = null;
		
		try {
			lTipoUsuarioDTO = objectMapper.readValue(prTipoUsuarioDTO, TipoUsuarioDTO.class);
			
			lTipoUsuarioDTO = lTipoUsuarioDao.saveTipoUsuario(lTipoUsuarioDTO);
		
			if(lTipoUsuarioDTO != null) {
				return new ResponseEntity<TipoUsuarioDTO>(lTipoUsuarioDTO,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("Se tuvo un error al guardar los datos del Tipo de Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			LOGGER.error("Error saveTipoUsuario: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al guardar los datos del Tipo de Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@PostMapping("/tipoUsuario/activateOrDesactivateTipoUsuario")
	public ResponseEntity<?> activateOrDesactivateTipoUsuario(String prTipoUsuarioId) {
		try {
			TipoUsuarioDTO lTipoUsuarioDTO = lTipoUsuarioDao.activateOrDesactivateTipoUsuario(prTipoUsuarioId);
			if(lTipoUsuarioDTO != null) {
				return new ResponseEntity<TipoUsuarioDTO>(lTipoUsuarioDTO,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("Se tuvo un error al actualizar los datos del Tipo de Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("Error activateOrDesactivateTipoUsuario: detalle: " + e.getMessage());
			return new ResponseEntity<String>("Se tuvo un error al actualizar los datos del Tipo de Usuario, favor de contactar al administrador.",HttpStatus.BAD_REQUEST);
		}
	}
	
}
