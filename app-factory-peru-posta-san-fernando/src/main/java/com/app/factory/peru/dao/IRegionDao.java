package com.app.factory.peru.dao;

import java.util.List;

import com.app.factory.peru.dto.RegionDTO;

public interface IRegionDao {
	public List<RegionDTO> getByFilter(String prRegionType, String prRegionID) throws Exception;
	public RegionDTO getByRegioNumero(int prRegioNumero) throws Exception;
}
