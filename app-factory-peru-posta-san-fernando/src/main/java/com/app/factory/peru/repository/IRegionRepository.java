package com.app.factory.peru.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.app.factory.peru.entity.RegionEntity;

public interface IRegionRepository extends CrudRepository<RegionEntity, Long>{
	
	@Query("FROM RegionEntity WHERE departamento IS NULL AND provincia IS NULL")
	List<RegionEntity> getByFilterDepartamento(String regionNumero);
	@Query("FROM RegionEntity WHERE departamento = (:regionNumero)")
	List<RegionEntity> getByFilterProvincia(@Param("regionNumero")String regionNumero);
	@Query("FROM RegionEntity WHERE provincia = (:regionNumero)")
	List<RegionEntity> getByFilterDistrito(@Param("regionNumero")String regionNumero);
	
	Optional<RegionEntity> findByRegionNumero(int regionNumero);
}
