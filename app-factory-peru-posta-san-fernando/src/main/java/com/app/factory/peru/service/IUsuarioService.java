package com.app.factory.peru.service;

import java.util.List;
import java.util.Optional;

import com.app.factory.peru.entity.UsuarioEntity;

public interface IUsuarioService {
	public List<UsuarioEntity> getAllUser();
	public UsuarioEntity add(UsuarioEntity prUsuarioEntity);
	public List<UsuarioEntity> getByFilter(UsuarioEntity prUsuarioEntity);
	public Optional<UsuarioEntity> getByUserId(String prUsuarioId);
	public UsuarioEntity getLoggedUser() throws Exception;
}
