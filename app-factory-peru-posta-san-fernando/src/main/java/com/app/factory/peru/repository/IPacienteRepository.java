package com.app.factory.peru.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.factory.peru.entity.PacienteEntity;

@Repository
public interface IPacienteRepository extends CrudRepository<PacienteEntity, Long> {
	
	@Query("FROM PacienteEntity WHERE nombre = IFNULL(?1,nombre) AND apellidoPaterno = IFNULL(?2,apellidoPaterno) "
			+ "AND apellidoMaterno = IFNULL(?3,apellidoMaterno) AND numeroDocumento = IFNULL(?4,numeroDocumento) AND estado = IFNULL(?5,estado)")
	List<PacienteEntity> findbyFilter(String nombre, String apePat,String apeMat, String nroDocumento, String estado);
	
	Optional<PacienteEntity> findByNumeroDocumento(String documento);
}
