package com.app.factory.peru.service.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.factory.peru.entity.RegionEntity;
import com.app.factory.peru.repository.IRegionRepository;
import com.app.factory.peru.service.IRegionService;

@Service
public class RegionService implements IRegionService{
	
	@Autowired
	private IRegionRepository lRegionRepository;

	@Override
	public List<RegionEntity> getByFilterDepartamento(String prRegionNumero) throws Exception {
		// TODO Auto-generated method stub
		return lRegionRepository.getByFilterDepartamento(prRegionNumero);
	}

	@Override
	public List<RegionEntity> getByFilterProvincia(String prRegionNumero) throws Exception {
		// TODO Auto-generated method stub
		return lRegionRepository.getByFilterProvincia(prRegionNumero);
	}

	@Override
	public List<RegionEntity> getByFilterDistrito(String prRegionNumero) throws Exception {
		// TODO Auto-generated method stub
		return lRegionRepository.getByFilterDistrito(prRegionNumero);
	}

	@Override
	public Optional<RegionEntity> getByRegioNumero(int prRegionNumero) throws Exception {
		// TODO Auto-generated method stub
		return lRegionRepository.findByRegionNumero(prRegionNumero);
	}

	
}
