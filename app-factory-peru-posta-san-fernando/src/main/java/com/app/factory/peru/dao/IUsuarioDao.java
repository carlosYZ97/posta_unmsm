package com.app.factory.peru.dao;

import java.util.List;

import com.app.factory.peru.dto.UsuarioDTO;

public interface IUsuarioDao {

	public List<UsuarioDTO> getAllUser();
	public UsuarioDTO saveUser(UsuarioDTO prUsuarioDTO) throws Exception;
	public List<UsuarioDTO> getByFilter(UsuarioDTO prUsuarioDTO) throws Exception;
	public UsuarioDTO activateOrDesactivateUser(String prUserId);
	
}
