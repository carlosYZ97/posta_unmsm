package com.app.factory.peru.service;

import java.util.List;
import java.util.Optional;
import com.app.factory.peru.entity.RolEntity;

public interface IRolService {
	
	public List<RolEntity> getAllRol() throws Exception;
	public RolEntity add(RolEntity prRolEntity) throws Exception;
	public List<RolEntity> getByFilter(RolEntity prRolEntity) throws Exception;
	public Optional<RolEntity> getByRolId(String prRolId) throws Exception;
	public List<RolEntity> findbyNombreRol(String keyword) throws Exception;
}
