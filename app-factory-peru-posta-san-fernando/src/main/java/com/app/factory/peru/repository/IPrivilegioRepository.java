package com.app.factory.peru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.app.factory.peru.entity.PrivilegioEntity;

public interface IPrivilegioRepository extends CrudRepository<PrivilegioEntity,Long>{

	@Query("FROM PrivilegioEntity WHERE nombre like %:keyword% AND estado = 1")
	List<PrivilegioEntity> findbyNombrePrivilegio(@Param("keyword") String keyword);
	
}
