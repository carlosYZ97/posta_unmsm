package com.app.factory.peru.dto;

import java.util.Date;
import java.util.Set;

public class RolDTO {
	
	private Long rolId;
	private String name;
	private String descripcion;
	private int estado;
	private String estadoDescripcion;
	private String creator;
	private Date created;
	private String createdDescripcion;
	private String changer;
	private Date changed;
	private String changedDescripcion;
	private Set<PrivilegioDTO> privilegios;
	
	public RolDTO() {
		super();
	}

	public RolDTO(Long rolId, String name, String descripcion, int estado, String estadoDescripcion, String creator,
			Date created, String createdDescripcion, String changer, Date changed, String changedDescripcion) {
		super();
		this.rolId = rolId;
		this.name = name;
		this.descripcion = descripcion;
		this.estado = estado;
		this.estadoDescripcion = estadoDescripcion;
		this.creator = creator;
		this.created = created;
		this.createdDescripcion = createdDescripcion;
		this.changer = changer;
		this.changed = changed;
		this.changedDescripcion = changedDescripcion;
	}

	public Long getRolId() {
		return rolId;
	}

	public void setRolId(Long rolId) {
		this.rolId = rolId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getEstadoDescripcion() {
		return estadoDescripcion;
	}

	public void setEstadoDescripcion(String estadoDescripcion) {
		this.estadoDescripcion = estadoDescripcion;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getCreatedDescripcion() {
		return createdDescripcion;
	}

	public void setCreatedDescripcion(String createdDescripcion) {
		this.createdDescripcion = createdDescripcion;
	}

	public String getChanger() {
		return changer;
	}

	public void setChanger(String changer) {
		this.changer = changer;
	}

	public Date getChanged() {
		return changed;
	}

	public void setChanged(Date changed) {
		this.changed = changed;
	}

	public String getChangedDescripcion() {
		return changedDescripcion;
	}

	public void setChangedDescripcion(String changedDescripcion) {
		this.changedDescripcion = changedDescripcion;
	}
	
	public Set<PrivilegioDTO> getPrivilegios() {
		return privilegios;
	}

	public void setPrivilegios(Set<PrivilegioDTO> privilegios) {
		this.privilegios = privilegios;
	}

	@Override
	public String toString() {
		return "RolDTO [rolId=" + rolId + ", name=" + name + ", descripcion=" + descripcion + ", estado=" + estado
				+ ", estadoDescripcion=" + estadoDescripcion + ", creator=" + creator + ", created=" + created
				+ ", createdDescripcion=" + createdDescripcion + ", changer=" + changer + ", changed=" + changed
				+ ", changedDescripcion=" + changedDescripcion + "]";
	}

}
