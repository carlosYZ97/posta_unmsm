package com.app.factory.peru.service;

import java.util.List;

import com.app.factory.peru.entity.PrivilegioEntity;

public interface IPrivilegioService {
	
	public List<PrivilegioEntity> findbyNombrePrivilegio(String keyword) throws Exception;
	
}
