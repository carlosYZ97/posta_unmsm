package com.app.factory.peru.dao.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.app.factory.peru.converter.PrivilegioConverter;
import com.app.factory.peru.dao.IPrivilegioDao;
import com.app.factory.peru.dto.PrivilegioDTO;
import com.app.factory.peru.service.IPrivilegioService;

@Component("privilegioDao")
public class PrivilegioDao implements IPrivilegioDao{

	@Autowired
	@Qualifier("privilegioConverter")
	private PrivilegioConverter privilegioConverter;

	@Autowired
	private IPrivilegioService privilegioService;
	
	@Override
	public List<PrivilegioDTO> findbyNombrePrivilegio(String keyword) throws Exception {
		List<PrivilegioDTO> lPrivilegioDTOCollection = new ArrayList<>();
		this.privilegioService.findbyNombrePrivilegio(keyword).stream().forEach(c -> {
			lPrivilegioDTOCollection.add(this.privilegioConverter.entityToDTO(c));
		});
		return lPrivilegioDTOCollection;
	}

}
