package com.app.factory.peru.converter;

import org.springframework.stereotype.Component;
import com.app.factory.peru.dto.PrivilegioDTO;
import com.app.factory.peru.entity.PrivilegioEntity;

@Component("privilegioConverter")
public class PrivilegioConverter {

	public PrivilegioEntity dtoToEntity(PrivilegioDTO prPrivilegioDTO) throws Exception {

		PrivilegioEntity lPrivilegioEntity = new PrivilegioEntity();
		lPrivilegioEntity.setPrivilegioId(prPrivilegioDTO.getPrivilegioId());
		lPrivilegioEntity.setNombre(prPrivilegioDTO.getNombre());
		lPrivilegioEntity.setDescripcion(prPrivilegioDTO.getDescripcion());
		lPrivilegioEntity.setEstado(prPrivilegioDTO.getEstado());
		
		return lPrivilegioEntity;
	}

	public PrivilegioDTO entityToDTO(PrivilegioEntity prPrivilegioEntity) {

		PrivilegioDTO lPrivilegioDTO = new PrivilegioDTO();

		lPrivilegioDTO.setPrivilegioId(prPrivilegioEntity.getPrivilegioId());
		lPrivilegioDTO.setNombre(prPrivilegioEntity.getNombre());
		lPrivilegioDTO.setDescripcion(prPrivilegioEntity.getDescripcion());
		lPrivilegioDTO.setEstado(prPrivilegioEntity.getEstado());
		lPrivilegioDTO.setEstadoDescripcion((prPrivilegioEntity.getEstado() == 0) ? "Inactivo" : "Activo");

		return lPrivilegioDTO;
	}
	
}
