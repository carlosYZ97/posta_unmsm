package com.app.factory.peru.service.ServiceImpl;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.User;

import com.app.factory.peru.entity.RolEntity;
import com.app.factory.peru.entity.UsuarioEntity;
import com.app.factory.peru.repository.IUsuarioRepository;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	IUsuarioRepository lUsuarioRepository;
	private static final Logger LOGGER = LogManager.getLogger(UserDetailsServiceImpl.class);

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LOGGER.warn("user name " + username);
		UsuarioEntity lUsuarioEntity = lUsuarioRepository.findByUsuarioAndEstado(username, 1)
				.orElseThrow(() -> new UsernameNotFoundException("User does not exist!"));
		Set<GrantedAuthority> grantList = new HashSet<>();
		for (RolEntity role : lUsuarioEntity.getRoles()) {
			GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getDescripcion());
			grantList.add(grantedAuthority);
		}

		LOGGER.warn("user entity " + lUsuarioEntity);
		UserDetails user = (UserDetails) new User(lUsuarioEntity.getUsuario(), lUsuarioEntity.getContrasena(),
				grantList);
		LOGGER.warn("user " + user);
		return user;
	}

}
