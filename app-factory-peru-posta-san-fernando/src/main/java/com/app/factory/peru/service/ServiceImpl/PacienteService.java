package com.app.factory.peru.service.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.factory.peru.entity.PacienteEntity;
import com.app.factory.peru.repository.IPacienteRepository;
import com.app.factory.peru.service.IPacienteService;


@Service
public class PacienteService implements IPacienteService{
	
	@Autowired
	private IPacienteRepository lPacienteRepository;
	
	@Override
	public List<PacienteEntity> getAllPatient(){
		return (List<PacienteEntity>) lPacienteRepository.findAll();
	}
	
	@Override
	public PacienteEntity add(PacienteEntity prPacienteEntity) {
		return lPacienteRepository.save(prPacienteEntity);
	}
	
	@Override
	public List<PacienteEntity> getByFilter(PacienteEntity prPacienteEntity) {
		
		return lPacienteRepository.findbyFilter((prPacienteEntity.getNombre().isEmpty()) ? null : prPacienteEntity.getNombre(),
				(prPacienteEntity.getApellidoPaterno().isEmpty()) ? null : prPacienteEntity.getApellidoPaterno(),
				(prPacienteEntity.getApellidoMaterno().isEmpty()) ? null : prPacienteEntity.getApellidoMaterno(),
				(prPacienteEntity.getNumeroDocumento().isEmpty()) ? null : prPacienteEntity.getNumeroDocumento(),
				(prPacienteEntity.getEstado() > -1) ? String.valueOf(prPacienteEntity.getEstado()) : null);
		
	}

	@Override
	public Optional<PacienteEntity> getByPatientId(String prPacienteId) {
		// TODO Auto-generated method stub
		return lPacienteRepository.findById(Long.parseLong(prPacienteId));
	}

	@Override
	public Optional<PacienteEntity> getByPatientDocument(String prPacienteDocument) {
		return lPacienteRepository.findByNumeroDocumento(prPacienteDocument);
	}
}
