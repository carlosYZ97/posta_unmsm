package com.app.factory.peru.dao;

import java.util.List;
import com.app.factory.peru.dto.TipoUsuarioDTO;

public interface ITipoUsuarioDao {
	public List<TipoUsuarioDTO> getAllTipoUsuario() throws Exception;
	public TipoUsuarioDTO saveTipoUsuario(TipoUsuarioDTO prTipoUsuarioDTO) throws Exception;
	public List<TipoUsuarioDTO> getByFilter(TipoUsuarioDTO prTipoUsuarioDTO) throws Exception;
	public TipoUsuarioDTO activateOrDesactivateTipoUsuario(String prTipoUsuarioId) throws Exception;
	public List<TipoUsuarioDTO> findbyNombreTipoUsuario(String keyword) throws Exception;
}
