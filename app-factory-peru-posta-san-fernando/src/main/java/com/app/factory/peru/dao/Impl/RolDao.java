package com.app.factory.peru.dao.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.app.factory.peru.converter.RolConverter;
import com.app.factory.peru.dao.IRolDao;
import com.app.factory.peru.dto.RolDTO;
import com.app.factory.peru.entity.RolEntity;
import com.app.factory.peru.entity.UsuarioEntity;
import com.app.factory.peru.service.IRolService;
import com.app.factory.peru.service.ServiceImpl.UsuarioService;

@Component("rolDao")
public class RolDao implements IRolDao {

	@Autowired
	@Qualifier("rolConverter")
	private RolConverter rolConverter;
	
	@Autowired
	private UsuarioService lUsuarioService;

	@Autowired
	private IRolService rolService;

	@Override
	public List<RolDTO> getAllRol() throws Exception {
		List<RolDTO> lRolDTOCollection = new ArrayList<>();
		this.rolService.getAllRol().stream().forEach(c -> {
			lRolDTOCollection.add(this.rolConverter.entityToDTO(c));
		});
		return lRolDTOCollection;
	}

	@Override
	public RolDTO saveRol(RolDTO prRolDTO) throws Exception {

		RolDTO lRolDTO = null;
		RolEntity rolEntity = rolConverter.dtoToEntity(prRolDTO);
		UsuarioEntity lUsuarioEntity = lUsuarioService.getLoggedUser();
		rolEntity.setCreator(lUsuarioEntity.getUsuario());
		rolEntity.setCreated(new Date());

		lRolDTO = rolConverter.entityToDTO(rolService.add(rolEntity));

		return lRolDTO;
	}

	@Override
	public List<RolDTO> getByFilter(RolDTO prRolDTO) throws Exception {
		List<RolDTO> lRolDTOCollection = new ArrayList<>();
		RolEntity rolEntity = rolConverter.dtoToEntity(prRolDTO);
		this.rolService.getByFilter(rolEntity).stream().forEach(c -> {
			lRolDTOCollection.add(this.rolConverter.entityToDTO(c));
		});
		return lRolDTOCollection;
	}

	@Override
	public RolDTO activateOrDesactivateRol(String prRolId) throws Exception {
		java.util.Optional<RolEntity> optionalRolEntity = rolService.getByRolId(prRolId);
		RolDTO lRolDTO = null;
		if (optionalRolEntity.isPresent()) {
			RolEntity rolEntity = optionalRolEntity.get();
			rolEntity.setEstado((rolEntity.getEstado() == 0) ? 1 : 0);
			rolEntity.setChanged(new Date());
			UsuarioEntity lUsuarioEntity = lUsuarioService.getLoggedUser();
			rolEntity.setChanger(lUsuarioEntity.getUsuario());
			lRolDTO = rolConverter.entityToDTO(rolService.add(rolEntity));
		}

		return lRolDTO;
	}

	@Override
	public List<RolDTO> findbyNombreRol(String keyword) throws Exception {
		List<RolDTO> lRolDTOCollection = new ArrayList<>();
		this.rolService.findbyNombreRol(keyword).stream().forEach(c -> {
			lRolDTOCollection.add(this.rolConverter.entityToDTO(c));
		});
		return lRolDTOCollection;
	}

}
