package com.app.factory.peru.service;

import java.util.List;
import java.util.Optional;

import com.app.factory.peru.entity.PacienteEntity;

public interface IPacienteService {
	public List<PacienteEntity> getAllPatient();
	public PacienteEntity add(PacienteEntity prPacienteEntity);
	public  List<PacienteEntity> getByFilter(PacienteEntity prPacienteEntity);
	public Optional<PacienteEntity> getByPatientId(String prPacienteId);
	public Optional<PacienteEntity> getByPatientDocument(String prPacienteDocument);	
}
	
