package com.app.factory.peru.dto;

public class PrivilegioDTO {
	
	private Long privilegioId;
	private String nombre;
	private String descripcion;
	private int estado;
	private String estadoDescripcion;
	
	public Long getPrivilegioId() {
		return privilegioId;
	}
	public void setPrivilegioId(Long privilegioId) {
		this.privilegioId = privilegioId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getEstadoDescripcion() {
		return estadoDescripcion;
	}
	public void setEstadoDescripcion(String estadoDescripcion) {
		this.estadoDescripcion = estadoDescripcion;
	}
	
	
}
