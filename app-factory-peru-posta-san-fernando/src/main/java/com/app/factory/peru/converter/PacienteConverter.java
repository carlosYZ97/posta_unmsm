package com.app.factory.peru.converter;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;

import com.app.factory.peru.dto.PacienteDTO;
import com.app.factory.peru.dto.RegionDTO;
import com.app.factory.peru.entity.PacienteEntity;

@Component("pacienteConverter")
public class PacienteConverter {
	
	public PacienteEntity dtoToEntity(PacienteDTO prPacienteDTO) throws Exception{
		
		PacienteEntity lPacienteEntity = new PacienteEntity();
		RegionConverter regionConverter = new RegionConverter();
		RegionDTO lRegionDTO = new RegionDTO();
		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat lSimpleDateFormatV2 = new SimpleDateFormat("dd/MM/yyyy");
		
		lPacienteEntity.setPacienteId(prPacienteDTO.getPacienteId());
		lPacienteEntity.setNombre(prPacienteDTO.getNombre());
		lPacienteEntity.setApellidoPaterno(prPacienteDTO.getApellidoPaterno());
		lPacienteEntity.setApellidoMaterno(prPacienteDTO.getApellidoMaterno());
		lPacienteEntity.setNumeroDocumento(prPacienteDTO.getNumeroDocumento());
		lPacienteEntity.setSexo(prPacienteDTO.getSexo());
		lPacienteEntity.setEstadoCivil(prPacienteDTO.getEstadoCivil());
		lPacienteEntity.setLugarNacimiento(prPacienteDTO.getLugarNacimiento());
		lPacienteEntity.setPeso(prPacienteDTO.getPeso());
		lPacienteEntity.setTalla(prPacienteDTO.getTalla());
		lPacienteEntity.setDireccionActual(prPacienteDTO.getDireccionActual());
		lPacienteEntity.setRegionId(regionConverter.dtoToEntity(lRegionDTO));
//		lPacienteEntity.setRegionId(regionConverter.dtoToEntity(prPacienteDTO.getRegionId()));
		lPacienteEntity.setTelefono(prPacienteDTO.getTelefono());
		lPacienteEntity.setCelular(prPacienteDTO.getCelular());
		lPacienteEntity.setCorreo(prPacienteDTO.getCorreo());
		lPacienteEntity.setNombreContacto(prPacienteDTO.getNombreContacto());
		lPacienteEntity.setTelefonoContacto(prPacienteDTO.getTelefonoContacto());
		lPacienteEntity.setCorreoContacto(prPacienteDTO.getCorreoContacto());
		lPacienteEntity.setEstado(prPacienteDTO.getEstado());
		lPacienteEntity.setOcupacion(prPacienteDTO.getOcupacion());
		lPacienteEntity.setGradoInstruccion(prPacienteDTO.getGradoInstruccion());
		lPacienteEntity.setRaza(prPacienteDTO.getRaza());
		lPacienteEntity.setReligion(prPacienteDTO.getReligion());
		lPacienteEntity.setNacionalidad(prPacienteDTO.getNacionalidad());
		if(prPacienteDTO.getCreatedDescripcion() != null && !prPacienteDTO.getCreatedDescripcion().isEmpty() && prPacienteDTO.getCreated() == null) {
			lPacienteEntity.setCreated(lSimpleDateFormat.parse(prPacienteDTO.getCreatedDescripcion()));
		}
		
		if(prPacienteDTO.getFechaNacimientoDescripcion() != null && !prPacienteDTO.getFechaNacimientoDescripcion().isEmpty()) {
			lPacienteEntity.setFechaNacimiento(lSimpleDateFormatV2.parse(prPacienteDTO.getFechaNacimientoDescripcion()));
		}
		
		lPacienteEntity.setCreator(prPacienteDTO.getCreator());
		
		return lPacienteEntity;
	}
	
	public PacienteDTO entityToDTO( PacienteEntity prPacienteEntity) throws Exception {
		
		PacienteDTO lPacienteDTO = new PacienteDTO();
		RegionConverter regionConverter = new RegionConverter();
		
		lPacienteDTO.setPacienteId(prPacienteEntity.getPacienteId());
		lPacienteDTO.setNombre(prPacienteEntity.getNombre());
		lPacienteDTO.setApellidoPaterno(prPacienteEntity.getApellidoPaterno());
		lPacienteDTO.setApellidoMaterno(prPacienteEntity.getApellidoMaterno());
		lPacienteDTO.setNumeroDocumento(prPacienteEntity.getNumeroDocumento());
		lPacienteDTO.setSexo(prPacienteEntity.getSexo());
		lPacienteDTO.setEstadoCivil(prPacienteEntity.getEstadoCivil());
		lPacienteDTO.setLugarNacimiento(prPacienteEntity.getLugarNacimiento());
		lPacienteDTO.setPeso(prPacienteEntity.getPeso());
		lPacienteDTO.setTalla(prPacienteEntity.getTalla());
		lPacienteDTO.setDireccionActual(prPacienteEntity.getDireccionActual());
		lPacienteDTO.setRegionId(regionConverter.entityToDTO(prPacienteEntity.getRegionId()));
		lPacienteDTO.setTelefono(prPacienteEntity.getTelefono());
		lPacienteDTO.setCelular(prPacienteEntity.getCelular());
		lPacienteDTO.setCorreo(prPacienteEntity.getCorreo());
		lPacienteDTO.setNombreContacto(prPacienteEntity.getNombreContacto());
		lPacienteDTO.setTelefonoContacto(prPacienteEntity.getTelefonoContacto());
		lPacienteDTO.setCorreoContacto(prPacienteEntity.getCorreoContacto());
		lPacienteDTO.setOcupacion(prPacienteEntity.getOcupacion());
		lPacienteDTO.setGradoInstruccion(prPacienteEntity.getGradoInstruccion());
		lPacienteDTO.setRaza(prPacienteEntity.getRaza());
		lPacienteDTO.setReligion(prPacienteEntity.getReligion());
		lPacienteDTO.setNacionalidad(prPacienteEntity.getNacionalidad());
		lPacienteDTO.setEstado(prPacienteEntity.getEstado());	
		lPacienteDTO.setEstadoDescripcion((prPacienteEntity.getEstado() == 0)? "Inactivo" : "Activo");
		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		lPacienteDTO.setCreatedDescripcion(lSimpleDateFormat.format(prPacienteEntity.getCreated()));		
		SimpleDateFormat lSimpleDateFormatV2 = new SimpleDateFormat("dd/MM/yyyy");
		if(prPacienteEntity.getFechaNacimiento() != null) {
			lPacienteDTO.setFechaNacimientoDescripcion(lSimpleDateFormatV2.format(prPacienteEntity.getFechaNacimiento()));
		}
		return lPacienteDTO;
	}
}
