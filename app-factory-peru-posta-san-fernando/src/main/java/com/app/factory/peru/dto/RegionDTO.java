package com.app.factory.peru.dto;

public class RegionDTO {
	
	private long regionId;
	private int regionNumero;
	private String departamento;
	private String provincia;
	private String nombre;
	
	public RegionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegionDTO(long regionId, int regionNumero, String departamento, String provincia, String nombre) {
		super();
		this.regionId = regionId;
		this.regionNumero = regionNumero;
		this.departamento = departamento;
		this.provincia = provincia;
		this.nombre = nombre;
	}

	public long getRegionId() {
		return regionId;
	}

	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}

	public int getRegionNumero() {
		return regionNumero;
	}

	public void setRegionNumero(int regionNumero) {
		this.regionNumero = regionNumero;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "RegionDTO [regionId=" + regionId + ", regionNumero=" + regionNumero + ", departamento=" + departamento
				+ ", provincia=" + provincia + ", nombre=" + nombre + "]";
	}

}
