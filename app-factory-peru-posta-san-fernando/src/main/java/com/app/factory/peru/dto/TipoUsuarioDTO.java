package com.app.factory.peru.dto;

import java.util.Date;

public class TipoUsuarioDTO {
	
	private Long tipoUsuarioId;
	private String nombre;
	private String descripcion;
	private int estado;
	private String estadoDescripcion;
	private String creator;
	private Date created;
	private String createdDescripcion;
	private String changer;
	private Date changed;
	private String changedDescripcion;
	
	public Long getTipoUsuarioId() {
		return tipoUsuarioId;
	}
	public void setTipoUsuarioId(Long tipoUsuarioId) {
		this.tipoUsuarioId = tipoUsuarioId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getCreatedDescripcion() {
		return createdDescripcion;
	}
	public void setCreatedDescripcion(String createdDescripcion) {
		this.createdDescripcion = createdDescripcion;
	}
	public String getChanger() {
		return changer;
	}
	public void setChanger(String changer) {
		this.changer = changer;
	}
	public Date getChanged() {
		return changed;
	}
	public void setChanged(Date changed) {
		this.changed = changed;
	}
	public String getChangedDescripcion() {
		return changedDescripcion;
	}
	public void setChangedDescripcion(String changedDescripcion) {
		this.changedDescripcion = changedDescripcion;
	}
	public String getEstadoDescripcion() {
		return estadoDescripcion;
	}
	public void setEstadoDescripcion(String estadoDescripcion) {
		this.estadoDescripcion = estadoDescripcion;
	}
	@Override
	public String toString() {
		return "TipoUsuarioDTO [tipoUsuarioId=" + tipoUsuarioId + ", nombre=" + nombre + ", descripcion=" + descripcion
				+ ", estado=" + estado + ", estadoDescripcion=" + estadoDescripcion + ", creator=" + creator
				+ ", created=" + created + ", createdDescripcion=" + createdDescripcion + ", changer=" + changer
				+ ", changed=" + changed + ", changedDescripcion=" + changedDescripcion + "]";
	}

}
