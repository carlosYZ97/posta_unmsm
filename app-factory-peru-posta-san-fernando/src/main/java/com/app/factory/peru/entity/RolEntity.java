package com.app.factory.peru.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ROL")
public class RolEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ROLID")
	private Long rolId;
	@Column(name = "NAME")
	private String name;
	@Column(name = "DESCRIPCION")
	private String descripcion;
	@Column(name = "ESTADO")
	private int estado;
	@Column(name = "CREATOR")
	private String creator;
	@Column(name = "CREATED")
	private Date created;
	@Column(name = "CHANGER")
	private String changer;
	@Column(name = "CHANGED")
	private Date changed;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ROLPRIVILEGIO",
			joinColumns=@JoinColumn(name="ROLID"),
			inverseJoinColumns=@JoinColumn(name="PRIVILEGIOID"))
	private Set<PrivilegioEntity> privilegios;

	public RolEntity() {
		super();
	}

	public RolEntity(Long rolId, String name, String descripcion, int estado, String creator, Date created,
			String changer, Date changed) {
		super();
		this.rolId = rolId;
		this.name = name;
		this.descripcion = descripcion;
		this.estado = estado;
		this.creator = creator;
		this.created = created;
		this.changer = changer;
		this.changed = changed;
	}

	public Long getRolId() {
		return rolId;
	}

	public void setRolId(Long rolId) {
		this.rolId = rolId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getChanger() {
		return changer;
	}

	public void setChanger(String changer) {
		this.changer = changer;
	}

	public Date getChanged() {
		return changed;
	}

	public void setChanged(Date changed) {
		this.changed = changed;
	}
	
	public Set<PrivilegioEntity> getPrivilegios() {
		return privilegios;
	}

	public void setPrivilegios(Set<PrivilegioEntity> privilegios) {
		this.privilegios = privilegios;
	}

	@Override
	public String toString() {
		return "RolEntity [rolId=" + rolId + ", name=" + name + ", descripcion=" + descripcion + ", estado=" + estado
				+ ", creator=" + creator + ", created=" + created + ", changer=" + changer + ", changed=" + changed
				+ "]";
	}

	private static final long serialVersionUID = 4149692960235315643L;

}
