package com.app.factory.peru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.app.factory.peru.entity.RolEntity;

public interface IRolRepository extends CrudRepository<RolEntity, Long>{

	@Query("FROM RolEntity WHERE name = IFNULL(?1,name) AND descripcion = IFNULL(?2,descripcion) AND estado = IFNULL(?3,estado)")
	List<RolEntity> findbyFilter(String name,String descripcion, String estado);
	
	@Query("FROM RolEntity WHERE name like %:keyword% AND estado = 1")
	List<RolEntity> findbyNombreRol(@Param("keyword") String keyword);
}
