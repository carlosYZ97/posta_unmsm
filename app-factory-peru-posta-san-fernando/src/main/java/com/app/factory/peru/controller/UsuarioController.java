package com.app.factory.peru.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.factory.peru.dao.IUsuarioDao;
import com.app.factory.peru.dto.UsuarioDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class UsuarioController {
	private static final Logger LOGGER = LogManager.getLogger(UsuarioController.class);
	@Autowired
	@Qualifier("usuarioDao")
	private IUsuarioDao lUsuarioDao;
	
	@Autowired
    private ObjectMapper objectMapper;
	
	@PostMapping("/user/userList")
	public List<UsuarioDTO> getUserList(String prUsuarioDTO) {
		
		UsuarioDTO lUsuarioDTO = null;
		try {
			
			lUsuarioDTO = objectMapper.readValue(prUsuarioDTO, UsuarioDTO.class);
			return lUsuarioDao.getByFilter(lUsuarioDTO);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.warn("getUserList : " + e.getMessage());
		}
		
		return null;
	}
	
	@PostMapping("/user/userAdd")
	public UsuarioDTO getUserAdd(String prUserDTO) {
		
		UsuarioDTO lUsuarioDTO = null;
		try {
			lUsuarioDTO = objectMapper.readValue(prUserDTO, UsuarioDTO.class);
			
			lUsuarioDTO.setCreator("cyunca");
			System.out.println(lUsuarioDTO);
			lUsuarioDao.saveUser(lUsuarioDTO);
				
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lUsuarioDTO;
	}
	
	@PostMapping("/user/activateOrDesactivateUser")
	public UsuarioDTO activateOrDesactivateUser(String prUserId) {
	
		return lUsuarioDao.activateOrDesactivateUser(prUserId);
	}
	
	@PostMapping("/user/userRolAdd")
	public UsuarioDTO getUserAddRol(String prUserDTO) {
		
		UsuarioDTO lUsuarioDTO = null;
		try {
			lUsuarioDTO = objectMapper.readValue(prUserDTO, UsuarioDTO.class);
			
			lUsuarioDTO.setCreator("cyunca");
			LOGGER.warn("usuario : " + prUserDTO);
			System.out.println(lUsuarioDTO);
			lUsuarioDao.saveUser(lUsuarioDTO);
				
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lUsuarioDTO;
	}
	
}
