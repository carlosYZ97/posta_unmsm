package com.app.factory.peru.dao.Impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.app.factory.peru.converter.UsuarioConverter;
import com.app.factory.peru.dao.IUsuarioDao;
import com.app.factory.peru.dto.UsuarioDTO;
import com.app.factory.peru.entity.RolEntity;
import com.app.factory.peru.entity.UsuarioEntity;
import com.app.factory.peru.service.IUsuarioService;

@Component("usuarioDao")
public class UsuarioDao implements IUsuarioDao {
	
	private static final Logger LOGGER = LogManager.getLogger(UsuarioDao.class);

	@Autowired
	@Qualifier("usuarioConverter")
	private UsuarioConverter usuarioConverter;

	@Autowired
	private IUsuarioService usuarioService;

	@Override
	public List<UsuarioDTO> getAllUser() {

		List<UsuarioDTO> lUsuarioDTOCollection = new ArrayList<>();

		this.usuarioService.getAllUser().stream().forEach(c -> {
			lUsuarioDTOCollection.add(this.usuarioConverter.entityToDTO(c));
		});

		return lUsuarioDTOCollection;
	}

	@Override
	public UsuarioDTO saveUser(UsuarioDTO prUsuarioDTO) throws Exception {

		UsuarioDTO lUsuarioDTO = null;
		try {
			UsuarioEntity usuarioEntity = usuarioConverter.dtoToEntity(prUsuarioDTO);

			usuarioEntity.setCreated(new Date());

			lUsuarioDTO = usuarioConverter.entityToDTO(usuarioService.add(usuarioEntity));

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return lUsuarioDTO;
	}

	@Override
	public List<UsuarioDTO> getByFilter(UsuarioDTO prUsuarioDTO) throws Exception {
		
		List<UsuarioDTO> lUsuarioDTOCollection = new ArrayList<>();
		try {
			UsuarioEntity usuarioEntity = usuarioConverter.dtoToEntity(prUsuarioDTO);
			
			this.usuarioService.getByFilter(usuarioEntity).stream().forEach(c -> {
				lUsuarioDTOCollection.add(this.usuarioConverter.entityToDTO(c));
			});
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lUsuarioDTOCollection;
	}

	@Override
	public UsuarioDTO activateOrDesactivateUser(String prUserId) {
		
		java.util.Optional<UsuarioEntity> optionalUsuarioEntity = usuarioService.getByUserId(prUserId);
		UsuarioDTO lUsuarioDTO = null;
		if(optionalUsuarioEntity.isPresent()) {
			UsuarioEntity usuarioEntity = optionalUsuarioEntity.get(); 
			usuarioEntity.setEstado((usuarioEntity.getEstado() == 0)? 1 : 0);
			usuarioEntity.setChanged(new Date());
			usuarioEntity.setChanger("cm");
			for (RolEntity iterable_element : usuarioEntity.getRoles()) {
				LOGGER.warn("rol name " + iterable_element.getName());
			}
			lUsuarioDTO = usuarioConverter.entityToDTO(usuarioService.add(usuarioEntity));
		}
		
		return lUsuarioDTO;
	}

}
