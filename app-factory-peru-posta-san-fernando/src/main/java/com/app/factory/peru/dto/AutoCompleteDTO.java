package com.app.factory.peru.dto;

public class AutoCompleteDTO {
	
	private Long Id;
	private String Name;
	private String Image;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long Id) {
		this.Id = Id;
	}
	public String getName() {
		return Name;
	}
	public void setName(String Name) {
		this.Name = Name;
	}
	public String getImage() {
		return Image;
	}
	public void setImage(String Image) {
		this.Image = Image;
	}
	
	
}
