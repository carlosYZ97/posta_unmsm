package com.app.factory.peru.service.ServiceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.app.factory.peru.entity.UsuarioEntity;
import com.app.factory.peru.repository.IUsuarioRepository;
import com.app.factory.peru.service.IUsuarioService;

@Service
public class UsuarioService implements IUsuarioService {

	@Autowired
	private IUsuarioRepository lUsuarioRepository;

	@Override
	public List<UsuarioEntity> getAllUser() {
		return (List<UsuarioEntity>) lUsuarioRepository.findAll();
	}

	@Override
	public UsuarioEntity add(UsuarioEntity prUsuarioEntity) {
		return lUsuarioRepository.save(prUsuarioEntity);
	}

	@Override
	public List<UsuarioEntity> getByFilter(UsuarioEntity prUsuarioEntity) {
		return lUsuarioRepository.findbyFilter(
				(prUsuarioEntity.getUsuario().isEmpty()) ? null : prUsuarioEntity.getUsuario(),
				(prUsuarioEntity.getNombre().isEmpty()) ? null : prUsuarioEntity.getNombre(),
				(prUsuarioEntity.getApellidoPaterno().isEmpty()) ? null : prUsuarioEntity.getApellidoPaterno(),
				(prUsuarioEntity.getApellidoMaterno().isEmpty()) ? null : prUsuarioEntity.getApellidoMaterno(),
				(prUsuarioEntity.getNumeroDocumento().isEmpty()) ? null : prUsuarioEntity.getNumeroDocumento(),
				(prUsuarioEntity.getEstado() > -1) ? String.valueOf(prUsuarioEntity.getEstado()) : null);
	}

	@Override
	public Optional<UsuarioEntity> getByUserId(String prUsuarioId) {
		return lUsuarioRepository.findById(Long.parseLong(prUsuarioId));
	}

	@Override
	public UsuarioEntity getLoggedUser() throws Exception {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		UserDetails loggedUser = null;

		//Verificar que ese objeto traido de sesion es el usuario
		if (principal instanceof UserDetails) {
			loggedUser = (UserDetails) principal;
		}
		
		UsuarioEntity myUser = lUsuarioRepository
				.findByUsuario(loggedUser.getUsername()).orElseThrow(() -> new Exception("Error obteniendo el usuario logeado desde la sesion."));
		
		return myUser;
	}
}
