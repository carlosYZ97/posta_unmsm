package com.app.factory.peru.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USUARIO")
public class UsuarioEntity implements Serializable {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="USUARIOID")
	private Long usuarioId;
	@Column(name="USUARIO")
	private String usuario;
	@Column(name="CONTRASENA")
	private String contrasena;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="APELLIDOPATERNO")
	private String apellidoPaterno;
	@Column(name="APELLIDOMATERNO")
	private String apellidoMaterno;
	@Column(name="NUMERODOCUMENTO")
	private String numeroDocumento;
	@Column(name="FECHANACIMIENTO")
	private Date fechaNacimiento;
	@Column(name="CORREO")
	private String correo;
	@Column(name="TELEFONO")
	private String telefono;
	@Column(name="CELULAR")
	private String celular;
	@Column(name="ESTADO")
	private int estado;
	@Column(name="CREATOR")
	private String creator;
	@Column(name="CREATED")
	private Date created;
	@Column(name="CHANGER")
	private String changer;
	@Column(name="CHANGED")
	private Date changed;
	@Column(name="TIPOUSUARIOID")
	private int tipousuario;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "USUARIOSROLES",
			joinColumns=@JoinColumn(name="USUARIOID"),
			inverseJoinColumns=@JoinColumn(name="ROLID"))
	private Set<RolEntity> roles;

	public UsuarioEntity() {
		super();
		// TODO Auto-generated constructor stub
	}


	public UsuarioEntity(Long usuarioId, String usuario, String contrasena, String nombre, String apellidoPaterno,
			String apellidoMaterno, String numeroDocumento, Date fechaNacimiento, String correo, String telefono,
			String celular, int estado, String creator, Date created, String changer, Date changed) {
		super();
		this.usuarioId = usuarioId;
		this.usuario = usuario;
		this.contrasena = contrasena;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.numeroDocumento = numeroDocumento;
		this.fechaNacimiento = fechaNacimiento;
		this.correo = correo;
		this.telefono = telefono;
		this.celular = celular;
		this.estado = estado;
		this.creator = creator;
		this.created = created;
		this.changer = changer;
		this.changed = changed;
	}


	public Long getUsuarioId() {
		return usuarioId;
	}


	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getContrasena() {
		return contrasena;
	}


	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidoPaterno() {
		return apellidoPaterno;
	}


	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}


	public String getApellidoMaterno() {
		return apellidoMaterno;
	}


	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}


	public String getNumeroDocumento() {
		return numeroDocumento;
	}


	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}


	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public String getCorreo() {
		return correo;
	}


	public void setCorreo(String correo) {
		this.correo = correo;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public int getEstado() {
		return estado;
	}


	public void setEstado(int estado) {
		this.estado = estado;
	}


	public String getCreator() {
		return creator;
	}


	public void setCreator(String creator) {
		this.creator = creator;
	}


	public Date getCreated() {
		return created;
	}


	public void setCreated(Date created) {
		this.created = created;
	}


	public String getChanger() {
		return changer;
	}


	public void setChanger(String changer) {
		this.changer = changer;
	}


	public Date getChanged() {
		return changed;
	}


	public void setChanged(Date changed) {
		this.changed = changed;
	}

	public int getTipousuario() {
		return tipousuario;
	}


	public void setTipousuario(int tipousuario) {
		this.tipousuario = tipousuario;
	}
	
	public Set<RolEntity> getRoles() {
		return roles;
	}

	public void setRoles(Set<RolEntity> roles) {
		this.roles = roles;
	}

	
	@Override
	public String toString() {
		return "UsuarioEntity [usuarioId=" + usuarioId + ", usuario=" + usuario + ", contrasena=" + contrasena
				+ ", nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno
				+ ", numeroDocumento=" + numeroDocumento + ", fechaNacimiento=" + fechaNacimiento + ", correo=" + correo
				+ ", telefono=" + telefono + ", celular=" + celular + ", estado=" + estado + ", creator=" + creator
				+ ", created=" + created + ", changer=" + changer + ", changed=" + changed + "]";
	}	
	
	
	private static final long serialVersionUID = 1952044519636141355L;
	
}
