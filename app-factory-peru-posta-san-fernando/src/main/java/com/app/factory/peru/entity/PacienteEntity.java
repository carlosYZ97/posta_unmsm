package com.app.factory.peru.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PACIENTE")
public class PacienteEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PACIENTEID")
	private Long pacienteId;
	@Column(name="NOMBRE")
	private String nombre;
	@Column(name="APELLIDOPATERNO")
	private String apellidoPaterno;
	@Column(name="APELLIDOMATERNO")
	private String apellidoMaterno;
	@Column(name="NUMERODOCUMENTO")
	private String numeroDocumento;
	@Column(name="SEXO")
	private int sexo;	
	@Column(name="FECHANACIMIENTO")
	private Date fechaNacimiento;
	@Column(name="LUGARNACIMIENTO")
	private String lugarNacimiento;	
	@Column(name="PESO")
	private double peso;
	@Column(name="TALLA")
	private double talla;
	@Column(name="RAZA")
	private String raza;
	@Column(name="RELIGION")
	private String religion;
	@Column(name="NACIONALIDAD")
	private String nacionalidad;
	@Column(name="TELEFONO")
	private String telefono;
	@Column(name="CELULAR")
	private String celular;
	@Column(name="CORREO")
	private String correo;
	@Column(name="GRADOINSTRUCCION")
	private int gradoInstruccion;
	@Column(name="OCUPACION")
	private String ocupacion;
	@Column(name="ESTADOCIVIL")
	private int estadoCivil;
	@Column(name="DIRECCIONACTUAL")
	private String direccionActual;
	@OneToOne
	@JoinColumn(name = "REGIONID")
	private RegionEntity regionId;	
//	@Column(name="REGIONID")
//	private Long regionId;
	@Column(name="NOMBRECONTACTO")
	private String nombreContacto;
	@Column(name="TELEFONOCONTACTO")
	private String telefonoContacto;
	@Column(name="CORREOCONTACTO")
	private String correoContacto;
	@Column(name="ESTADO")
	private int estado;
	@Column(name="CREATOR")
	private String creator;
	@Column(name="CREATED")
	private Date created;
	@Column(name="CHANGER")
	private String changer;
	@Column(name="CHANGED")
	private Date changed;
	
	public PacienteEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public PacienteEntity(Long pacienteId, String nombre, String apellidoPaterno, String apellidoMaterno,
			String numeroDocumento, int sexo, Date fechaNacimiento, String lugarNacimiento, double peso, double talla,
			String raza, String religion, String nacionalidad, String telefono, String celular, String correo,
			int gradoInstruccion, String ocupacion, int estadoCivil, String direccionActual, RegionEntity regionId,
			String nombreContacto, String telefonoContacto, String correoContacto, int estado, String creator,
			Date created, String changer, Date changed) {
		super();
		this.pacienteId = pacienteId;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.numeroDocumento = numeroDocumento;
		this.sexo = sexo;
		this.fechaNacimiento = fechaNacimiento;
		this.lugarNacimiento = lugarNacimiento;
		this.peso = peso;
		this.talla = talla;
		this.raza = raza;
		this.religion = religion;
		this.nacionalidad = nacionalidad;
		this.telefono = telefono;
		this.celular = celular;
		this.correo = correo;
		this.gradoInstruccion = gradoInstruccion;
		this.ocupacion = ocupacion;
		this.estadoCivil = estadoCivil;
		this.direccionActual = direccionActual;
		this.regionId = regionId;
		this.nombreContacto = nombreContacto;
		this.telefonoContacto = telefonoContacto;
		this.correoContacto = correoContacto;
		this.estado = estado;
		this.creator = creator;
		this.created = created;
		this.changer = changer;
		this.changed = changed;
	}

	public Long getPacienteId() {
		return pacienteId;
	}



	public void setPacienteId(Long pacienteId) {
		this.pacienteId = pacienteId;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getApellidoPaterno() {
		return apellidoPaterno;
	}



	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}



	public String getApellidoMaterno() {
		return apellidoMaterno;
	}



	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}



	public String getNumeroDocumento() {
		return numeroDocumento;
	}



	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}



	public int getSexo() {
		return sexo;
	}



	public void setSexo(int sexo) {
		this.sexo = sexo;
	}



	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}



	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}



	public String getLugarNacimiento() {
		return lugarNacimiento;
	}



	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}



	public double getPeso() {
		return peso;
	}



	public void setPeso(double peso) {
		this.peso = peso;
	}



	public double getTalla() {
		return talla;
	}



	public void setTalla(double talla) {
		this.talla = talla;
	}



	public String getRaza() {
		return raza;
	}



	public void setRaza(String raza) {
		this.raza = raza;
	}



	public String getReligion() {
		return religion;
	}



	public void setReligion(String religion) {
		this.religion = religion;
	}



	public String getNacionalidad() {
		return nacionalidad;
	}



	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}



	public String getTelefono() {
		return telefono;
	}



	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}



	public String getCelular() {
		return celular;
	}



	public void setCelular(String celular) {
		this.celular = celular;
	}



	public String getCorreo() {
		return correo;
	}



	public void setCorreo(String correo) {
		this.correo = correo;
	}



	public int getGradoInstruccion() {
		return gradoInstruccion;
	}



	public void setGradoInstruccion(int gradoInstruccion) {
		this.gradoInstruccion = gradoInstruccion;
	}



	public String getOcupacion() {
		return ocupacion;
	}



	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}



	public int getEstadoCivil() {
		return estadoCivil;
	}



	public void setEstadoCivil(int estadoCivil) {
		this.estadoCivil = estadoCivil;
	}



	public String getDireccionActual() {
		return direccionActual;
	}



	public void setDireccionActual(String direccionActual) {
		this.direccionActual = direccionActual;
	}



//	public Long getRegionId() {
//		return regionId;
//	}
//
//
//
//	public void setRegionId(Long regionId) {
//		this.regionId = regionId;
//	}
	

	public String getNombreContacto() {
		return nombreContacto;
	}



	public RegionEntity getRegionId() {
		return regionId;
	}

	public void setRegionId(RegionEntity regionId) {
		this.regionId = regionId;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}



	public String getTelefonoContacto() {
		return telefonoContacto;
	}



	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}



	public String getCorreoContacto() {
		return correoContacto;
	}



	public void setCorreoContacto(String correoContacto) {
		this.correoContacto = correoContacto;
	}



	public int getEstado() {
		return estado;
	}



	public void setEstado(int estado) {
		this.estado = estado;
	}



	public String getCreator() {
		return creator;
	}



	public void setCreator(String creator) {
		this.creator = creator;
	}



	public Date getCreated() {
		return created;
	}



	public void setCreated(Date created) {
		this.created = created;
	}



	public String getChanger() {
		return changer;
	}



	public void setChanger(String changer) {
		this.changer = changer;
	}



	public Date getChanged() {
		return changed;
	}



	public void setChanged(Date changed) {
		this.changed = changed;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "PacienteEntity [pacienteId=" + pacienteId + ", nombre=" + nombre + ", apellidoPaterno="
				+ apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno + ", numeroDocumento=" + numeroDocumento
				+ ", sexo=" + sexo + ", fechaNacimiento=" + fechaNacimiento + ", lugarNacimiento=" + lugarNacimiento
				+ ", peso=" + peso + ", talla=" + talla + ", raza=" + raza + ", religion=" + religion
				+ ", nacionalidad=" + nacionalidad + ", telefono=" + telefono + ", celular=" + celular + ", correo="
				+ correo + ", gradoInstruccion=" + gradoInstruccion + ", ocupacion=" + ocupacion + ", estadoCivil="
				+ estadoCivil + ", direccionActual=" + direccionActual + ", regionId=" + regionId + ", nombreContacto="
				+ nombreContacto + ", telefonoContacto=" + telefonoContacto + ", correoContacto=" + correoContacto
				+ ", estado=" + estado + ", creator=" + creator + ", created=" + created + ", changer=" + changer
				+ ", changed=" + changed + "]";
	}



	private static final long serialVersionUID = 1952044519636141355L;
}
