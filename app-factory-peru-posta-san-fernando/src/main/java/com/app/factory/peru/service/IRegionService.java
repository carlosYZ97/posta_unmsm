package com.app.factory.peru.service;
import java.util.List;
import java.util.Optional;

import com.app.factory.peru.entity.RegionEntity;

public interface IRegionService {
	public List<RegionEntity> getByFilterDepartamento(String prRegionNumero) throws Exception;
	public List<RegionEntity> getByFilterProvincia(String prRegionNumero) throws Exception;
	public List<RegionEntity> getByFilterDistrito(String prRegionNumero) throws Exception;
	
	public Optional<RegionEntity> getByRegioNumero(int prRegionNumero) throws Exception;
}
