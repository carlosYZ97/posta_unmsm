package com.app.factory.peru.converter;

import org.springframework.stereotype.Component;

import com.app.factory.peru.dto.RegionDTO;
import com.app.factory.peru.entity.RegionEntity;

@Component("regionConverter")
public class RegionConverter {
	
	public RegionEntity dtoToEntity(RegionDTO prRegionDTO) throws Exception{
		
		RegionEntity lRegionEntity = new RegionEntity();
		
		lRegionEntity.setRegionId(prRegionDTO.getRegionId());
		lRegionEntity.setRegionNumero(prRegionDTO.getRegionNumero());
		lRegionEntity.setDepartamento(prRegionDTO.getDepartamento());
		lRegionEntity.setProvincia(prRegionDTO.getProvincia());
		lRegionEntity.setNombre(prRegionDTO.getNombre());	
		
		return lRegionEntity;
	}
	
	public RegionDTO entityToDTO(RegionEntity prRegionEntity) throws Exception{
		
		RegionDTO lRegionDTO = new RegionDTO();
		
		lRegionDTO.setRegionId(prRegionEntity.getRegionId());
		lRegionDTO.setRegionNumero(prRegionEntity.getRegionNumero());
		lRegionDTO.setDepartamento(prRegionEntity.getDepartamento());
		lRegionDTO.setProvincia(prRegionEntity.getProvincia());
		lRegionDTO.setNombre(prRegionEntity.getNombre());
		
		return lRegionDTO;
	}
}
