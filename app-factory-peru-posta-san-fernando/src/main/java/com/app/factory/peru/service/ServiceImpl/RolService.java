package com.app.factory.peru.service.ServiceImpl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.factory.peru.entity.RolEntity;
import com.app.factory.peru.repository.IRolRepository;
import com.app.factory.peru.service.IRolService;

@Service
public class RolService implements IRolService{

	@Autowired
	private IRolRepository lRolRepository;
	
	@Override
	public List<RolEntity> getAllRol() throws Exception{
		return (List<RolEntity>) lRolRepository.findAll();
	}

	@Override
	public RolEntity add(RolEntity prRolEntity) throws Exception {
		return lRolRepository.save(prRolEntity);
	}

	@Override
	public List<RolEntity> getByFilter(RolEntity prRolEntity) throws Exception{
		return lRolRepository.findbyFilter(
				(prRolEntity.getName().isEmpty()) ? null : prRolEntity.getName(),
				(prRolEntity.getDescripcion().isEmpty()) ? null : prRolEntity.getDescripcion(),
				(prRolEntity.getEstado() > -1) ? String.valueOf(prRolEntity.getEstado()) : null);
	}

	@Override
	public Optional<RolEntity> getByRolId(String prRolId) throws Exception {
		return lRolRepository.findById(Long.parseLong(prRolId));
	}

	@Override
	public List<RolEntity> findbyNombreRol(String keyword) throws Exception {
		return lRolRepository.findbyNombreRol(keyword);
	}

}
