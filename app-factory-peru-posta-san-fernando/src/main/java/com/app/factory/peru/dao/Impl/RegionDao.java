package com.app.factory.peru.dao.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.app.factory.peru.converter.RegionConverter;
import com.app.factory.peru.dao.IRegionDao;
import com.app.factory.peru.dto.RegionDTO;
import com.app.factory.peru.entity.RegionEntity;
import com.app.factory.peru.service.IRegionService;

@Component("regionDao")
public class RegionDao implements IRegionDao{
	
	@Autowired
	@Qualifier("regionConverter")
	private RegionConverter regionConverter;
	
	@Autowired
	private IRegionService regionService;

	@Override
	public List<RegionDTO> getByFilter(String prRegionType, String prRegionID) throws Exception {
		// TODO Auto-generated method stub
		List<RegionDTO> lRegionDTOCollection = new ArrayList<>();
			if(prRegionType.equals("0")) {
				this.regionService.getByFilterDepartamento(prRegionID).stream().forEach(c -> {			
					try {
						lRegionDTOCollection.add(this.regionConverter.entityToDTO(c));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				});
			}else if(prRegionType.equals("1")) {
				this.regionService.getByFilterProvincia(prRegionID).stream().forEach(c -> {			
					try {
						lRegionDTOCollection.add(this.regionConverter.entityToDTO(c));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				});
			}else if(prRegionType.equals("2")) {
				this.regionService.getByFilterDistrito(prRegionID).stream().forEach(c -> {			
					try {
						lRegionDTOCollection.add(this.regionConverter.entityToDTO(c));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				});
			}
		return lRegionDTOCollection;
	}

	@Override
	public RegionDTO getByRegioNumero(int prRegioNumero) throws Exception {
		java.util.Optional<RegionEntity> optionalRegionEntity = regionService.getByRegioNumero(prRegioNumero);
		RegionDTO lRegionDTO = null;
		if(optionalRegionEntity.isPresent()) {
			RegionEntity regionEntity = optionalRegionEntity.get();
			lRegionDTO = regionConverter.entityToDTO(regionEntity);
		}
		return lRegionDTO;
	}
	
}
