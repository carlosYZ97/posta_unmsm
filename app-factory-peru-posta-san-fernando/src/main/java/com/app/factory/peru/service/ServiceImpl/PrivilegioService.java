package com.app.factory.peru.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.factory.peru.entity.PrivilegioEntity;
import com.app.factory.peru.repository.IPrivilegioRepository;
import com.app.factory.peru.service.IPrivilegioService;

@Service
public class PrivilegioService implements IPrivilegioService{

	@Autowired
	private IPrivilegioRepository lPrivilegioRepository;
	
	@Override
	public List<PrivilegioEntity> findbyNombrePrivilegio(String keyword) throws Exception {

		return lPrivilegioRepository.findbyNombrePrivilegio(keyword);
	}

}
