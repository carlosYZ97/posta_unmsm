package com.app.factory.peru.dto;

import java.util.Date;

public class PacienteDTO {
	
	private Long pacienteId;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String numeroDocumento;
	private int sexo;	
	private String lugarNacimiento;
	private Date fechaNacimiento;
	private double peso;
	private double talla;
	private String raza;
	private String religion;
	private String nacionalidad;	
	private String telefono;
	private String celular;
	private String correo;
	private int gradoInstruccion;
	private String ocupacion;
	private int estadoCivil;
	private String fechaNacimientoDescripcion;
	private String direccionActual;
//	private Long regionId;
	private RegionDTO regionId;
	private String nombreContacto;
	private String telefonoContacto;
	private String correoContacto;
	private int estado;
	private String estadoDescripcion;
	private String creator;
	private Date created;
	private String createdDescripcion;
	private String changer;
	private Date changed;
	private String changedDescripcion;
	
	public PacienteDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PacienteDTO(Long pacienteId, String nombre, String apellidoPaterno, String apellidoMaterno,
			String numeroDocumento, int sexo, String lugarNacimiento, Date fechaNacimiento, double peso, double talla,
			String raza, String religion, String nacionalidad, String telefono, String celular, String correo,
			int gradoInstruccion, String ocupacion, int estadoCivil, String fechaNacimientoDescripcion,
			String direccionActual, RegionDTO regionId, String nombreContacto, String telefonoContacto,
			String correoContacto, int estado, String estadoDescripcion, String creator, Date created,
			String createdDescripcion, String changer, Date changed, String changedDescripcion) {
		super();
		this.pacienteId = pacienteId;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.numeroDocumento = numeroDocumento;
		this.sexo = sexo;
		this.lugarNacimiento = lugarNacimiento;
		this.fechaNacimiento = fechaNacimiento;
		this.peso = peso;
		this.talla = talla;
		this.raza = raza;
		this.religion = religion;
		this.nacionalidad = nacionalidad;
		this.telefono = telefono;
		this.celular = celular;
		this.correo = correo;
		this.gradoInstruccion = gradoInstruccion;
		this.ocupacion = ocupacion;
		this.estadoCivil = estadoCivil;
		this.fechaNacimientoDescripcion = fechaNacimientoDescripcion;
		this.direccionActual = direccionActual;
		this.regionId = regionId;
		this.nombreContacto = nombreContacto;
		this.telefonoContacto = telefonoContacto;
		this.correoContacto = correoContacto;
		this.estado = estado;
		this.estadoDescripcion = estadoDescripcion;
		this.creator = creator;
		this.created = created;
		this.createdDescripcion = createdDescripcion;
		this.changer = changer;
		this.changed = changed;
		this.changedDescripcion = changedDescripcion;
	}

	public Long getPacienteId() {
		return pacienteId;
	}

	public void setPacienteId(Long pacienteId) {
		this.pacienteId = pacienteId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public int getSexo() {
		return sexo;
	}

	public void setSexo(int sexo) {
		this.sexo = sexo;
	}

	public String getLugarNacimiento() {
		return lugarNacimiento;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getTalla() {
		return talla;
	}

	public void setTalla(double talla) {
		this.talla = talla;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public int getGradoInstruccion() {
		return gradoInstruccion;
	}

	public void setGradoInstruccion(int gradoInstruccion) {
		this.gradoInstruccion = gradoInstruccion;
	}

	public String getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	public int getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(int estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getFechaNacimientoDescripcion() {
		return fechaNacimientoDescripcion;
	}

	public void setFechaNacimientoDescripcion(String fechaNacimientoDescripcion) {
		this.fechaNacimientoDescripcion = fechaNacimientoDescripcion;
	}

	public String getDireccionActual() {
		return direccionActual;
	}

	public void setDireccionActual(String direccionActual) {
		this.direccionActual = direccionActual;
	}

//	public Long getRegionId() {
//		return regionId;
//	}
//
//	public void setRegionId(Long regionId) {
//		this.regionId = regionId;
//	}
	

	public String getNombreContacto() {
		return nombreContacto;
	}

	public RegionDTO getRegionId() {
		return regionId;
	}

	public void setRegionId(RegionDTO regionId) {
		this.regionId = regionId;
	}

	public void setNombreContacto(String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}

	public String getTelefonoContacto() {
		return telefonoContacto;
	}

	public void setTelefonoContacto(String telefonoContacto) {
		this.telefonoContacto = telefonoContacto;
	}

	public String getCorreoContacto() {
		return correoContacto;
	}

	public void setCorreoContacto(String correoContacto) {
		this.correoContacto = correoContacto;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getEstadoDescripcion() {
		return estadoDescripcion;
	}

	public void setEstadoDescripcion(String estadoDescripcion) {
		this.estadoDescripcion = estadoDescripcion;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getCreatedDescripcion() {
		return createdDescripcion;
	}

	public void setCreatedDescripcion(String createdDescripcion) {
		this.createdDescripcion = createdDescripcion;
	}

	public String getChanger() {
		return changer;
	}

	public void setChanger(String changer) {
		this.changer = changer;
	}

	public Date getChanged() {
		return changed;
	}

	public void setChanged(Date changed) {
		this.changed = changed;
	}

	public String getChangedDescripcion() {
		return changedDescripcion;
	}

	public void setChangedDescripcion(String changedDescripcion) {
		this.changedDescripcion = changedDescripcion;
	}

	@Override
	public String toString() {
		return "PacienteDTO [pacienteId=" + pacienteId + ", nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno
				+ ", apellidoMaterno=" + apellidoMaterno + ", numeroDocumento=" + numeroDocumento + ", sexo=" + sexo
				+ ", lugarNacimiento=" + lugarNacimiento + ", fechaNacimiento=" + fechaNacimiento + ", peso=" + peso
				+ ", talla=" + talla + ", raza=" + raza + ", religion=" + religion + ", nacionalidad=" + nacionalidad
				+ ", telefono=" + telefono + ", celular=" + celular + ", correo=" + correo + ", gradoInstruccion="
				+ gradoInstruccion + ", ocupacion=" + ocupacion + ", estadoCivil=" + estadoCivil
				+ ", fechaNacimientoDescripcion=" + fechaNacimientoDescripcion + ", direccionActual=" + direccionActual
				+ ", regionId=" + regionId + ", nombreContacto=" + nombreContacto + ", telefonoContacto="
				+ telefonoContacto + ", correoContacto=" + correoContacto + ", estado=" + estado
				+ ", estadoDescripcion=" + estadoDescripcion + ", creator=" + creator + ", created=" + created
				+ ", createdDescripcion=" + createdDescripcion + ", changer=" + changer + ", changed=" + changed
				+ ", changedDescripcion=" + changedDescripcion + "]";
	}

}
