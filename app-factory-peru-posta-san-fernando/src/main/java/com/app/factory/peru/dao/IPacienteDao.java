package com.app.factory.peru.dao;

import java.util.List;

import com.app.factory.peru.dto.PacienteDTO;

public interface IPacienteDao {
	
	public List<PacienteDTO> getAllPatient();
	public PacienteDTO savePatient(PacienteDTO prPacienteDTO) throws Exception;
	public List<PacienteDTO> getByFilter(PacienteDTO prPacienteDTO) throws Exception;
	public PacienteDTO activateOrDesactivatePatient(String prPatientId) throws Exception;
	public PacienteDTO getPacienteByDocument(String prPacienteDocument) throws Exception;
}
