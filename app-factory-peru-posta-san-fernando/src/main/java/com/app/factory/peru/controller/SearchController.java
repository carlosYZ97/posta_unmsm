package com.app.factory.peru.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.factory.peru.dao.IPrivilegioDao;
import com.app.factory.peru.dao.IRolDao;
import com.app.factory.peru.dto.AutoCompleteDTO;
import com.app.factory.peru.dto.PrivilegioDTO;
import com.app.factory.peru.dto.RolDTO;

@RestController
public class SearchController {

	private static final Logger LOGGER = LogManager.getLogger(SearchController.class);
	
	@Autowired
	@Qualifier("rolDao")
	private IRolDao lRolDao;
	
	@Autowired
	@Qualifier("privilegioDao")
	private IPrivilegioDao lPrivilegioDao;

	@PostMapping("/search/getRolesUser")
	public ResponseEntity<?> getRolesUsuarios(String searchText) {
		
		List<RolDTO> lcoRolDTO = null;
		List<AutoCompleteDTO> lcoAutoComplete = new ArrayList<AutoCompleteDTO>();
		LOGGER.warn("***** Inició getRolesUsuarios *****");
		LOGGER.warn("***** param : " + searchText);

		try {

			 lcoRolDTO = lRolDao.findbyNombreRol(searchText);

			for (RolDTO rolDTO : lcoRolDTO) {
				AutoCompleteDTO lAutoCompleteDTO = new AutoCompleteDTO();
				lAutoCompleteDTO.setId(rolDTO.getRolId());
				lAutoCompleteDTO.setName(rolDTO.getName());
				lAutoCompleteDTO.setImage(rolDTO.getName());
				lcoAutoComplete.add(lAutoCompleteDTO);
			}
			return new ResponseEntity<List<AutoCompleteDTO>>(lcoAutoComplete, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("Error getRolesUsuarios: detalle: " + e.getMessage());
			return new ResponseEntity<String>(
					"Se tuvo un error al obtener los Roles, favor de contactar al administrador.",
					HttpStatus.BAD_REQUEST);
		} finally {

			LOGGER.warn("***** Fin getRolesUsuarios *****");
		}

	}
	
	@PostMapping("/search/getPrivilegiosRol")
	public ResponseEntity<?> getPrivilegiosRoles(String searchText) {
		
		List<PrivilegioDTO> lcoPrivilegioDTO = null;
		List<AutoCompleteDTO> lcoAutoComplete = new ArrayList<AutoCompleteDTO>();
		LOGGER.warn("***** Inició getPrivilegiosRoles *****");
		LOGGER.warn("***** param : " + searchText);

		try {

			lcoPrivilegioDTO = lPrivilegioDao.findbyNombrePrivilegio(searchText);

			for (PrivilegioDTO privilegioDTO : lcoPrivilegioDTO) {
				AutoCompleteDTO lAutoCompleteDTO = new AutoCompleteDTO();
				lAutoCompleteDTO.setId(privilegioDTO.getPrivilegioId());
				lAutoCompleteDTO.setName(privilegioDTO.getNombre());
				lAutoCompleteDTO.setImage(privilegioDTO.getNombre());
				lcoAutoComplete.add(lAutoCompleteDTO);
			}
			return new ResponseEntity<List<AutoCompleteDTO>>(lcoAutoComplete, HttpStatus.OK);

		} catch (Exception e) {
			LOGGER.error("Error getPrivilegiosRoles: detalle: " + e.getMessage());
			return new ResponseEntity<String>(
					"Se tuvo un error al obtener los Privilegios, favor de contactar al administrador.",
					HttpStatus.BAD_REQUEST);
		} finally {

			LOGGER.warn("***** Fin getPrivilegiosRoles *****");
		}

	}
}
