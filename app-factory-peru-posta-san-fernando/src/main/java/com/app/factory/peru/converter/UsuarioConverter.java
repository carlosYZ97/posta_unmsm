package com.app.factory.peru.converter;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.app.factory.peru.dto.RolDTO;
import com.app.factory.peru.dto.UsuarioDTO;
import com.app.factory.peru.entity.RolEntity;
import com.app.factory.peru.entity.UsuarioEntity;
import com.app.factory.peru.util.Passgenerator;

@Component("usuarioConverter")
public class UsuarioConverter {

	@Autowired
	@Qualifier("rolConverter")
	private RolConverter lRolConverter;

	public UsuarioEntity dtoToEntity(UsuarioDTO prUsuarioDTO) throws Exception {

		UsuarioEntity lUsuarioEntity = new UsuarioEntity();
		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat lSimpleDateFormatV2 = new SimpleDateFormat("dd/MM/yyyy");

		lUsuarioEntity.setUsuarioId(prUsuarioDTO.getUsuarioId());
		lUsuarioEntity.setUsuario(prUsuarioDTO.getUsuario());

		lUsuarioEntity.setEstado(prUsuarioDTO.getEstado());
		lUsuarioEntity.setTipousuario(1);
		lUsuarioEntity.setApellidoPaterno(prUsuarioDTO.getApellidoPaterno());
		lUsuarioEntity.setApellidoMaterno(prUsuarioDTO.getApellidoMaterno());
		lUsuarioEntity.setCelular(prUsuarioDTO.getCelular());
		lUsuarioEntity.setNombre(prUsuarioDTO.getNombre());
		lUsuarioEntity.setNumeroDocumento(prUsuarioDTO.getNumeroDocumento());
		lUsuarioEntity.setCorreo(prUsuarioDTO.getCorreo());

		if (prUsuarioDTO.getFechaNacimientoDescripcion() != null
				&& !prUsuarioDTO.getFechaNacimientoDescripcion().isEmpty()) {
			lUsuarioEntity.setFechaNacimiento(lSimpleDateFormatV2.parse(prUsuarioDTO.getFechaNacimientoDescripcion()));
		}

		if (prUsuarioDTO.getCreatedDescripcion() != null && !prUsuarioDTO.getCreatedDescripcion().isEmpty()
				&& prUsuarioDTO.getCreated() == null) {
			
			lUsuarioEntity.setCreated(lSimpleDateFormat.parse(prUsuarioDTO.getCreatedDescripcion()));
			lUsuarioEntity.setContrasena(Passgenerator.encodePassword(prUsuarioDTO.getContrasena()));
		}else {
			lUsuarioEntity.setContrasena(prUsuarioDTO.getContrasena());
		}

		lUsuarioEntity.setCreator(prUsuarioDTO.getCreator());
		lUsuarioEntity.setTelefono(prUsuarioDTO.getTelefono());

		Set<RolEntity> rolEntity = new HashSet<RolEntity>();
		if (prUsuarioDTO.getRoles() != null ) {
			prUsuarioDTO.getRoles().stream().forEach(c -> {
				try {
					rolEntity.add(lRolConverter.dtoToEntity(c));
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			lUsuarioEntity.setRoles(rolEntity);
		}

		return lUsuarioEntity;
	}

	public UsuarioDTO entityToDTO(UsuarioEntity prUsuarioEntity) {

		UsuarioDTO lUsuarioDTO = new UsuarioDTO();

		lUsuarioDTO.setUsuarioId(prUsuarioEntity.getUsuarioId());
		lUsuarioDTO.setUsuario(prUsuarioEntity.getUsuario());
		lUsuarioDTO.setNombre(prUsuarioEntity.getNombre());
		lUsuarioDTO.setApellidoPaterno(prUsuarioEntity.getApellidoPaterno());
		lUsuarioDTO.setApellidoMaterno(prUsuarioEntity.getApellidoMaterno());
		lUsuarioDTO.setCorreo(prUsuarioEntity.getCorreo());
		lUsuarioDTO.setTelefono(prUsuarioEntity.getTelefono());
		lUsuarioDTO.setCreator(prUsuarioEntity.getCreator());
		lUsuarioDTO.setContrasena(prUsuarioEntity.getContrasena());
		lUsuarioDTO.setEstado(prUsuarioEntity.getEstado());
		lUsuarioDTO.setEstadoDescripcion((prUsuarioEntity.getEstado() == 0) ? "Inactivo" : "Activo");

		SimpleDateFormat lSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat lSimpleDateFormatV2 = new SimpleDateFormat("dd/MM/yyyy");

		lUsuarioDTO.setCreatedDescripcion(lSimpleDateFormat.format(prUsuarioEntity.getCreated()));
		lUsuarioDTO.setNumeroDocumento(prUsuarioEntity.getNumeroDocumento());
		lUsuarioDTO.setCelular(prUsuarioEntity.getCelular());
		if (prUsuarioEntity.getFechaNacimiento() != null) {
			lUsuarioDTO.setFechaNacimientoDescripcion(lSimpleDateFormatV2.format(prUsuarioEntity.getFechaNacimiento()));
		}
		Set<RolDTO> rolDTO = new HashSet<RolDTO>();
		if (prUsuarioEntity.getRoles() != null) {
			prUsuarioEntity.getRoles().stream().forEach(c -> {
				rolDTO.add(this.lRolConverter.entityToDTO(c));
			});

			lUsuarioDTO.setRoles(rolDTO);
		}

		return lUsuarioDTO;
	}
}
